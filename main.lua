
-- luacheck:globals luaX luaY lua_assert

local yueliang_path = "/yelua/yueliang-0.4.1/orig-5.1.3/"
local files = {
	"zio",
	"lex",
	"opcodes",
	"dump",
	"code",
	"parser",
}

for _, file in ipairs( files ) do
	dofile( yueliang_path .. "l" .. file .. ".lua" )
end

local lua_state = {}
luaX:init()

local empty_table = setmetatable( {}, {
	__newindex = function() end;
} )

--[[
Okay, we'll need a decent constraint system. One that will allow us
to do things like
```
-- this may be too much?
-- requires something like
-- a "multiple of" constraint
local foo = math.random( 1, 10 ) * 2
if foo == 3 then
	-- unreachable
end


local a, b
if something then
	a = 1
	b = 2
else
	a = 2
	b = 1
end

-- requires cross-variable
-- constraints
if a + b == 2 then
	-- unreachable
end
```
]]

-- TODO: remove Yueliang's source optimisations (x = x isn't a MOVE)
-- TODO: we also need to parse comments
-- TODO: @insignificant annotation for ignorable return values
--      However, for returned parameters (such as returning self)
--      or perhaps even upvalues, the function should be considered
--      insignificant by default
-- TODO: support all luacheck lints

--- Prints tables and other fun stuff.
-- @return nil
local function deep_print( value, indent, no_newline )
	indent = indent or 1
	local t = type( value )
	local INDENT_SIZE = 4

	if t == "table" then
		print( "{" )

		for k, v in pairs( value ) do
			io.write( ( " " ):rep( indent * INDENT_SIZE ) )

			if type( k ) == "string" and k:match( "^[%a_][%w_]*$" ) then
				io.write( k )
			else
				io.write( "[ " )
				deep_print( k, indent + 1, true )
				io.write( " ]" )
			end

			io.write( " = " )
			deep_print( v, indent + 1, true )
			print( ";" )
		end

		io.write( ( " " ):rep( ( indent - 1 ) * INDENT_SIZE ) .. "}" )

	elseif t == "string" then
		io.write( '"' .. value .. '"' )
	else
		io.write( value )
	end

	if not no_newline then
		print()
	end
end

--- Proxy to table.sort which returns the table.
-- @param tbl	The table to sort
-- @param comp	Comparator function
-- @see table.sort
-- @return `tbl`
local function sort( tbl, comp )
	table.sort( tbl, comp )
	return tbl
end

--- Extract a certain line from a string.
-- @param str	description
-- @param n	description
local function get_line( str, n )
	return str:gsub( ".-\n", "", n - 1 ):match( "(.-)\n" )
end

function lua_assert( test )
	if not test then error( "assertion failed!" ) end
end

--- Parse Lua source code.
-- @param str	The source string
-- @param filename	(Optional) Name of the file the source comes from
-- @return The fn object from luaY:parser()
local function parse( str, filename )
	return luaY:parser(
		lua_state,
		{
			p = 0;
			n = #str;
			data = str;
			reader = function() return "" end;
		},
		nil,
		filename or "unnamed"
	)
end

--- Retrieve useful metadata from an instruction.
-- @param fn	The function object retrieved from the parser
-- @param index	The index of the requested instruction
-- @return targets (an array of locations written to), sources
local function decode_instr( fn, index )
	local instr = fn.code[ index ] or empty_table

	--- Get the type of a constant.
	-- @param i	An index into the fn.k table
	-- @return The type of the constant, as returned by type()
	local function ktype( i )
		return type( ( fn.k[ i ] or empty_table ).value )
	end

	-- this is really memory inefficient, but <shrug>
	local decoded = {
		[ 0 ] = { { {			-- MOVE
			storage = "register";
			index = instr.A;
		} }, { {
			storage = "register";
			index = instr.B;
		} } },
		{ { {					-- LOADK
			storage = "register";
			index = instr.A;
		} }, { {
			storage = "constant";
			type = ktype( instr.Bx );
			index = instr.Bx;
		} } },
		{ { {					-- LOADBOOL
			storage = "register";
			type = "boolean";
			index = instr.A;
		} }, {
		} },
		{ {						-- LOADNIL
			-- targets will be added programmatically
		}, {
		} },
		{ { {					-- GETUPVAL
			storage = "register";
			index = instr.A;
		} }, { {
			storage = "upvalue";
			index = instr.B;
		} } },
		{ { {					-- GETGLOBAL
			storage = "register";
			index = instr.A;
		} }, { {
			storage = "global";
			index = instr.Bx;
		} } },
		{ { {					-- GETTABLE
			storage = "register";
			index = instr.A;
		} }, { {
			storage = "register";
			index = instr.B;
		}, {
			storage = "index";
			table = instr.B;
			index = instr.C;
		} } },
		{ { {					-- SETGLOBAL
			storage = "global";
			index = instr.Bx;
		} }, { {
			storage = "register";
			index = instr.A;
		} } },
		{ { {					-- SETUPVAL
			storage = "upvalue";
			index = instr.B;
		} }, { {
			storage = "register";
			index = instr.A;
		} } },
		{ { {					-- SETTABLE
			storage = "register";
			index = instr.A;
		}, {
			storage = "newindex";
			table = instr.A;
			index = instr.B;
		} }, { {
			-- TODO: is this right?
			storage = ( instr.C and instr.C > 256 )
					and "constant"
					or "register";
			-- FIXME: this isn't
			index = instr.C;
		} } },
	}

	if instr.OP == 3 then		-- LOADNIL
		-- add targets (range of registers)
		for i = instr.A, instr.B do
			table.insert( decoded[ instr.OP ][ 1 ], {
				storage = "register";
				type = "nil";
				index = i;
			} )
		end
	end

	local result = decoded[ instr.OP ] or empty_table
	return result[ 1 ] or empty_table, result[ 2 ] or empty_table
end

--- Analyse Lua source code.
-- @param str	The source string
-- @param filename	(Optional) Name of the file the source comes from
-- @return An array of available inspections.
local function lint( str, filename )
	local inspections = {}

	local fn = parse( str, filename )
	local code = fn.code
	local allowed_globals = {
		print = {
			type = "function";
			allow = "read";
			params = {
				{
					name = "...";
					-- TODO: __tostring inspections
					type = "any";
				},
			};
		};
	}

	local dataflow = {}

	local var_stacks = {}
	local var_info = {}

	deep_print( fn )

	--- Initialize a tracked variable using a source or target object from decode_instr().
	-- @param instr_action	description
	-- @return The computed index into var_stacks and var_info
	local function init_var( instr_action )
		local index = instr_action.storage .. ":" .. instr_action.index

		var_info  [ index ] = var_info  [ index ] or instr_action
		var_stacks[ index ] = var_stacks[ index ] or {}

		return index
	end

	for i = 0, #code do
		local targets, sources = decode_instr( fn, i )

		for j = 1, #sources do
			local index = init_var( sources[ j ] )
			table.insert( var_stacks[ index ], {
				"read",
				i,
				sources[ j ].index,

				sources = sources;
				targets = targets;
			} )
		end

		for j = 1, #targets do
			local index = init_var( targets[ j ] )

			assert(
				targets[ j ].storage ~= "constant",
				"Linter bug: mutating a constant"
			)

			table.insert( var_stacks[ index ], {
				"write",
				i,
				targets[ j ].index,

				sources = sources;
				targets = targets;
			} )
		end
	end

	for id, stack in pairs( var_stacks ) do
		local write = false

		for i = 1, #stack do
			if var_info[ id ].storage == "global" then
				local name = fn.k[ var_info[ id ].index ].value

				if not allowed_globals[ name ] then
					table.insert( inspections, {
						line = fn.lineinfo[ stack[ i ][ 2 ] ];
						str = get_line( str, fn.lineinfo[ stack[ i ][ 2 ] ] )
						     :gsub( "^%s*", "" );
						msg = ( stack[ i ][ 1 ] == "write" and "Setting" or "Accessing" )
							.. " a non-standard global variable `"
							.. name
							.. "`";
					} )
				end
			end

			if stack[ i ][ 1 ] == "write" then
				if var_info[ id ].storage == "global" then
					local name = fn.k[ var_info[ id ].index ].value

					if  allowed_globals[ name ]
					and allowed_globals[ name ].allow ~= "write" then
						table.insert( inspections, {
							line = fn.lineinfo[ stack[ i ][ 2 ] ];
							str = get_line( str, fn.lineinfo[ stack[ i ][ 2 ] ] )
							     :gsub( "^%s*", "" );
							msg = "Overriding a standard global variable `"
								.. name
								.. "`";
						} )
					end
				end

				if write or i == #stack then
					-- either the previous operation was also a write,
					-- or this is the last operation and it is a write
					local instr_index = stack[ i == #stack and i or i - 1 ][ 2 ]

					-- some hackery to report two errors in case
					-- both conditions are true
					local limit = ( var_info[ id ].storage ~= "register"
					                or not fn.locvars[ stack[ i ][ 3 ] ]
					                or fn.locvars[ stack[ i ][ 3 ] ].startpc < i )
					              and 0
					              or ( ( write and i == #stack ) and 2 or 1 )

					for _ = 1, limit do
						table.insert( inspections, {
							line = fn.lineinfo[ instr_index ];
							str = get_line( str, fn.lineinfo[ instr_index ] )
								:gsub( "^%s*", "" );
							msg = "Value assigned to variable `"
								.. ( fn.locvars[ stack[ i ][ 3 ] ] or {
									varname = "?register_" .. stack[ i ][ 3 ];
								} ).varname
								.. "` is never used";
						} )

						if stack[ i - 1 ] then
							instr_index = stack[ i - 1 ][ 2 ]
						end
					end
				end

				write = true
			else
				write = false
			end

			print( id, stack[ i ][ 1 ] )
		end
	end

	return inspections
end

-- deep_print( sort( lint[[
-- 	local unused

-- 	local foo, bar = 'asdf'
-- 	foo = some_global
-- 	x = foo
-- 	foo = x
-- 	foo = 3

-- 	-- TODO: detect "if statement has empty body"
-- 	if bar then
-- 		print = true
-- 	end
-- ]], function( this, that )
-- 	return this.line < that.line
-- end ) )

return {
	deep_print = deep_print;
	lint = lint;
	parse = parse;
}
