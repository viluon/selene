
local promise = {}
local promise_metatable = {
	__index = promise;
	__call = function( self )
		return self:evaluate()
	end;
}

--- Create a new promise.
-- Unlike in JavaScript, these promises are resolved
-- immediately, and are thus merely an utility for
-- graceful error handling.
-- @param fn	The promise's callback.
-- @return The new promise object.
function promise.new( fn )
	local p = setmetatable( { fn = fn; getters = {} }, promise_metatable )
	return p
end

--- Evaluate this promise.
-- @return self
function promise:evaluate()
	if self.evaluated then return end
	self.evaluated = true

	--- Resolve the promise to a concrete result, triggering getters.
	-- @param ...	Arguments passed to the callback(s)
	-- @return nil
	local function resolve( ... )
		if self.resolved then return end
		local result = { ... }

		for i = 1, #self.getters do
			result = { self.getters[ i ]( unpack( result ) ) }
		end

		self.resolved = true
	end

	local unhandled

	--- Reject the promise (causing an error), triggering a catcher (if there is one).
	-- @param ...	Arguments passed to the callback(s)
	-- @return nil
	local function reject( ... )
		if not self.catcher or not pcall( self.catcher, ... ) then
			unhandled = true
		end
	end

	xpcall( function()
		return self.fn( resolve, reject )
	end, function( ... )
		return reject( ... )
	end )

	if unhandled then
		error( "Unhandled error in promise", 2 )
	end

	return self
end

--- Add a getter to this promise.
-- @param getter  The function called with the previous getter's result,
--                or the original result of the promise, if no getters were called yet.
-- @return self
function promise:get( getter )
	self.getters[ #self.getters + 1 ] = getter
	return self
end

--- Add a catcher (error handler) to this promise.
-- @param catcher	The function called with arguments to reject() or error data from xpcall
-- @return self
function promise:catch( catcher )
	self.catcher = catcher
	return self
end

return promise
