
local utils = require "utils"
local settings = require "settings"
local shallow_copy = utils.shallow_copy
local theme = require "themes".advanced
local lang  = require "lang"

--- Get a fresh new scope stack.
-- @return The default scope
local function get_default_scope()
	return { "source", "lua" }
end

--- A super simple hash function.
-- @param str	The string to hash
-- @return A hash of the first 32 bytes of `str`
local function hash( str )
	local bytes = { str:byte() }
	local result = 0

	for i = 1, math.min( 32, #bytes ) do
		result = result + bytes[ i ] * 3 ^ i
	end

	return result
end

--- Process a line of source code and highlight it.
-- @param n	The number of the line to highlight
-- @return nil
local function line( buff, n )
	-- TODO it may happen that a line at the top
	--      suddenly drastically changes the scope,
	--      such as when adding a double quote.
	--      For these cases, it would be best to
	--      keep a "dirty line" index, which would
	--      trigger scope recomputation from a certain
	--      line all the way to EOF

	local lines = buff.lines

	if lines[ n ].no_rehighlight then
		return
	end

	local scope
	-- Find a line with a set scope
	for i = n, 1, -1 do
		if lines[ i ].scope then
			scope = shallow_copy( lines[ i ].scope )

			-- Fix lines from the scoped one to our own
			for j = i, n do
				lines[ j ].scope = shallow_copy( scope )
			end

			break
		end
	end

	if not scope then
		scope = get_default_scope()
	end

	local word = ""
	local formatted = {}
	local line_str = lines[ n ].str

	local i = 1
	local screen_space_index = 0
	while i <= #line_str do
		local c = line_str:sub( i, i )

		while c:match "[%w_]" do
			word = word .. c
			i = i + 1
			c = line_str:sub( i, i )
		end

		if #word > 0 then
			if lang.lua.keywords[ word ] then
				table.insert( formatted, { theme.highlight.keyword, word } )
			elseif word:match "^%d+$" then
				table.insert( formatted, { theme.highlight.number, word } )
			else
				table.insert( formatted, {
					theme.foreground,
					word
				} )
				-- table.insert( formatted, {
				-- 	theme.semantic_colours[ hash( word ) % #theme.semantic_colours + 1 ],
				-- 	word
				-- } )
			end

			screen_space_index = screen_space_index + #word

			word = ""
		end

		if c == "'" or c == '"' then
			local delim = c
			local start = i

			repeat
				i = i + 1
				c = line_str:sub( i, i )
			until c == delim or i > #line_str

			table.insert( formatted, { theme.highlight.string, line_str:sub( start, i ) } )
			screen_space_index = screen_space_index + i - start

		elseif c == "-" and i < #line_str and line_str:sub( i + 1, i + 1 ) == "-" then
			-- start comment
			local prefix, summary, suffix = line_str:sub( i, -1 )
				:match "^%s*(%-%-%-%s+)(..-)(%.?%s*)$"
			if summary then
				-- LuaDoc summary brief
				table.insert( formatted, { theme.highlight.comment, prefix } )
				table.insert( formatted, { theme.highlight.summary, summary } )
				table.insert( formatted, { theme.highlight.comment, suffix } )
			else
				table.insert( formatted, { theme.highlight.comment, line_str:sub( i, -1 ) } )
			end

			screen_space_index = screen_space_index + #line_str + 1 - i
			i = #line_str + 1

		elseif lang.lua.control_characters[ c ] then
			table.insert( formatted, { theme.highlight.control_character, c } )
			screen_space_index = screen_space_index + 1
		elseif c == "\t" then
			local length = buff.tab_width - screen_space_index % buff.tab_width
			local tab_str

			if settings.show_whitespace then
				tab_str = ( "-" ):rep( length - 1 ) .. string.char( 26 )
			else
				tab_str = ( " " ):rep( length )
			end

			table.insert( formatted, {
				theme.secondary,
				tab_str,
				length = length;
			} )

			screen_space_index = screen_space_index + length
		else
			table.insert( formatted, { theme.foreground, c } )
			screen_space_index = screen_space_index + 1
		end

		i = i + 1
	end

	if settings.show_whitespace then
		for j = #formatted, 1, -1 do
			if formatted[ j ][ 2 ]:find " " then
				local fragments_added = 0
				local fragment = table.remove( formatted, j )
				local str = fragment[ 2 ]
				local prev_split = 1

				for split in str:gmatch "() " do
					local piece = str:sub( prev_split, split - 1 )

					table.insert( formatted, j + fragments_added, { fragment[ 1 ], piece } )
					table.insert( formatted, j + fragments_added + 1, { theme.secondary, string.char( 28 ) } )

					fragments_added = fragments_added + 2
					prev_split = split + 1
				end

				table.insert( formatted, j + fragments_added, { fragment[ 1 ], str:sub( prev_split, -1 ) } )
			end
		end
	end

	lines[ n ].formatted = formatted
	return formatted
end

--- Highlight a buffer.
-- To be used as a coroutine.
-- @param buff	description
-- @return nil
local function buffer( buff, first_dirty, last_dirty )
	utils.log "Starting full buffer highlight with" ( buff.title, first_dirty, last_dirty )
	for i = first_dirty or 1, last_dirty or buff.n_lines do
		line( buff, i )
		coroutine.yield()
	end
end

return {
	line = line;
	buffer = buffer;
}
