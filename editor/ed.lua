
-- FIXME: move CC dependencies to a separate file and provide
--        implementations for lfs and OC as well

-- luacheck:globals term os.queueEvent os.pullEvent os.startTimer
-- luacheck:globals keys colours fs.find fs.isDir



-- !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! --
--   TODO: pri-A: Make Go To Anything go to anything   --
-- !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! !!! --

-- Refactoring (much needed):
	-- draw/redraw/render - wtf?
	-- we *really* need to redo rendering
	-- - some drawing functions have side-effects!
	-- actions should go into a library (completely)
	-- move keyboard shortucts out of ed.lua
	-- actually, get rid of ed.lua altogether
	-- (separate everything into logically named files)
	-- implement badass hooks and make all widgets plugins
	-- modularity FTW

-- separate backend/frontend?
-- We could just keep a few core libraries defining IPC
-- and exposing shared functionality

-- TODO: allow annotating function parameters as not usage-qualified
	-- (`assert(foo)` shouldn't be a usage of `foo`)
-- TODO?: instantly rewrite "not true" and "not false"
-- TODO: open files from GitHub/GitLab with smart search
-- TODO: Ctrl+PageUp/PageDown to jump to the up/bottom of the window
-- TODO: line-based and scope-based undo -- undo changes made to a function
-- TODO: error on undefined behaviour
	-- mutating a table while iterating over it
	-- find more cases!
-- TODO: warn on "else if" (should be "elseif")
-- TODO: warn on alignment across an indent boundary
	-- example:
	-- if x then
	-- 	  x = 3
	-- 	^~ here (won't match with different tab width)
-- FIXME: jump to line in current file in go to anything no longer works
-- FIXME: result navigation in go to anything no longer works
-- TODO: inspection action for string parameters
	-- "generalise to any type" -- generalises the type in the LuaDoc and adds tostring()
-- TODO: enable proxies/overrides for caret motions in certain buffers
	-- TODO: translate line-changing motions in the widget buffer to selection changes
-- TODO: from Chrome DevTools - Shift+PageUp - increment by 100, Shift+up - increment by 10
-- TODO: go to error (!fuzzy error/warning text match) (show "waiting for analysis" if incomplete)
	-- an exclamation mark at the end limits results to ones containing an error
-- TODO: panes - generalize buffers and allow layouting (a tree of panes)
-- FIXME: luacheck: lexer: don't give up on goto as a symbol
	-- generally we don't wanna stop on errors. Ever.
-- TODO: jumps should focus on the caret so that it is in the middle third of the screen
-- FIXME: try inserting "@@###::" in go to anything
-- TODO: Alt+up/down moves by methods in IDEA
-- TODO: context menus (+ clickable fragments)
	-- TODO: upload to Pastebin
-- TODO: lipsum here
-- TODO: history (undo/redo) (also learn how Vim does it)
-- TODO: autocomplete CC teletext characters
-- TODO: pgUp/Dwn should preserve relative scroll
-- TODO: Holy crap! Kate/KDevelop's inline notes are awesome!
	-- [note]: local x, y = 1[, nil]
-- TODO: Shift+click on a selection flips the caret (ST3)
-- TODO: Ctrl+Shift+drag to set put carets on multiple lines
-- TODO: motion.simple.left.otherwise.simple.up()
-- TODO: line insertions have huge performance issues (>1500 lines is already terrible)
	-- likely caused by rehighlights -- make them incremental!
		---> will need a way to deprecate highlighting below the wave (dirt FTW)
-- FIXME: luacheck: lexer: don't give up on invalid escape sequences
-- FIXME: nested motions can potentionally break _caret (and _buffer) -- custom stack?
-- TODO: use ChunkSpy to parse bytecode
-- TODO: correctly offset issues on a line (and don't keep them in a separate object)
-- TODO: rainbow parentheses
-- TODO: highlight paren/bracket/brace matching the one under the caret
-- TODO: split to more Desktox buffers
-- FIXME: warning highlighting (overlapping shaders)
-- TODO: autoindent
-- TODO: autocompletion
-- FIXME: edit.remove for multiple carets
-- FIXME: show_bytecode for multiple carets
-- TODO: align bytecode with first word boundary
-- TODO: better bytecode formatting - like ChunkSpy
-- FIXME: rewrite ugliness in rendering
-- FIXME: better handling of numberless lines
-- TODO: readonly lines
-- TODO: show_bytecode: function prototypes
-- TODO: file IO
-- TODO: command palette
-- FIXME: deal with handler's lack of :remove_child()
-- TODO: -- language=Markdown - and similar comments

-- longterm:
-- TODO: actions on fragments
	-- TODO: navigation in the bytecode view, expand/collapse constant list
-- TODO: extend or rewrite ChunkSpy for 5.2 and 5.3 bytecode
-- TODO: terminal -- os.execute( "luac", ... ) to get native BC listing
-- TODO: luafmt
-- TODO: parse LDoc and display it
-- TODO: DynASM support?
-- TODO: luacheck: warn about repetitively creating closures with no/identical upvalues
	-- (typically buff:map(function() ... end))
-- TODO: in-place documentation for Lua patterns
-- TODO: performance inspection: closure instantiation in a metamethod
	-- actually, detect this using a more general rule for closures with no upvalues
	-- to the first parent level (could be moved up)
-- TODO: pattern parsing
	-- TODO: highlight capture associated with loop variable under caret in gmatch() loops
	-- TODO: warn on '^' and '$' usage in gmatch()
-- TODO: tailcall inspections (could be a tailcall, returns more values (because of a tailcall))

--[[
maybe we could have like a .metadata field on the line
which would say that it shouldn't be saved or counted
like the normal lines

also widgets, damn it, another hack!
buffer_view_listener passes relevant events
onto widget.listener
--]]

local args = { ... }
if type( args[ 1 ] ) ~= "table" then
	error( "Selene must be run via sln.lua", 0 )
	return
end

local desktox     = require "desktox"
local luacheck    = require "luacheck"
local utils       = require "utils"
local lang        = require "lang"
local highlight   = require "highlight"
local themes      = require "themes"
local settings    = require "settings"
local env         = require "env"
local text_buffer = require "buffer"
local dirt        = require "dirt"
local motion      = require "motion".motion

local log = utils.log
local buffer = desktox.buffer
local handler = desktox.handler
local colours = buffer.colours
local shallow_copy = utils.shallow_copy

local max, min, floor = math.max, math.min, math.floor

local main_win = term.current()
local w, h = main_win.getSize()

log( "starting up, w =", w .. ", h =", h )

local screen            = buffer.new( 0, 1,     w, h - 2 )
local status_bar_buffer = buffer.new( 0, h - 1, w, 1 )
local widget_view       = buffer.new( 0, 0,     w, h - 2, screen )
local buffer_view       = buffer.new( 0, 0,     w, h - 2, screen ):import( desktox.buffer_dtt )
local tabs_view         = buffer.new( 0, 0,     w, 1 )
local tabs_view_listener = handler.new(
	tabs_view.x1,
	tabs_view.y1,
	tabs_view.width,
	tabs_view.height
)
local buffer_view_listener = handler.new(
	buffer_view.x1 + screen.x1,
	buffer_view.y1 + screen.y1,
	buffer_view.width,
	buffer_view.height
)
local status_bar_listener = handler.new(
	status_bar_buffer.x1,
	status_bar_buffer.y1,
	status_bar_buffer.width,
	status_bar_buffer.height
)

local main_listener = handler.new( 0, 0, w, h )
	:adopt( buffer_view_listener )
	:adopt( tabs_view_listener )
	:adopt( status_bar_listener )

local change_buffer, show_status, show_go_to_anything, fix_tabs_scroll,
      new_buffer, refresh_widget, render_fragments, open_file, show_autocompletion_popup,
      render_autocompletion_popup

local debug = ""

local line_number_sidebar_width = 0
local longest_line_width = 0
local scheduled_lints = {}
local filesystem = { files = {} }
local reindex_timer = os.startTimer( 0 )
local open_buffers = {}
local statuses = {}
local held = {}
local is_view_disabled = false
local selected_buffer
local tabs_scroll = 0
-- TODO: this *really* needs a better and more general implementation
local widget
local theme

local edit = {
	auto_balance = {
		[ "'" ] = "'";
		[ '"' ] = '"';
		[ "(" ] = ")";
		[ "[" ] = "]";
		[ "{" ] = "}";
	};
	auto_balance_inv = {};
}

for k, v in pairs( edit.auto_balance ) do
	edit.auto_balance_inv[ v ] = k
end

--- Change the theme in use.
-- @param new_theme	The name of the new theme
-- @return nil
local function change_theme( new_theme )
	dirt.full_repaint_required = true
	theme = themes[ new_theme ] or themes.advanced
end

--- Compare two issues, errors should go last.
-- @see table.sort
local function errors_last_comparator( a, _ )
	return a.code:sub( 1, 1 ) == "0"
end

--- Schedule a relint of a buffer.
-- @param buff	description
-- @return nil
local function schedule_lint( buff )
	scheduled_lints[ buff ] = os.startTimer( settings.relint_delay )
end

--- Schedule a re-highlight of a buffer.
-- @param buff	description
-- @param first_dirty	description
-- @param last_dirty	description
-- @return nil
local function schedule_highlight( buff, first_dirty, last_dirty )
	first_dirty = first_dirty or 1

	if buff.scheduled_highlight_data then
		if first_dirty >= buff.scheduled_highlight_data.first_dirty
		and (
			not last_dirty
			or not buff.scheduled_highlight_data.last_dirty
			or last_dirty <= buff.scheduled_highlight_data.last_dirty
		) then
			return
		end

		first_dirty = math.min( first_dirty, buff.scheduled_highlight_data.first_dirty )
		last_dirty  = ( last_dirty
				and buff.scheduled_highlight_data.last_dirty
				and math.max( last_dirty, buff.scheduled_highlight_data.last_dirty )
			) or buff.scheduled_highlight_data.last_dirty or last_dirty
	end

	buff.highlight_routine = coroutine.create( highlight.buffer )
	buff.scheduled_highlight_data = {
		first_dirty = first_dirty;
		last_dirty = last_dirty;
	}

	local ok, err = coroutine.resume( buff.highlight_routine, buff, first_dirty, last_dirty )

	if not ok then
		log "Failed to start highlight routine" ( err )
		buff.scheduled_highlight_data = nil
	end
end

--- Show an autocomplete popup.
-- @param buff	description
-- @param caret	description
function show_autocompletion_popup( buff, caret )
	if caret.autocompletion_popup then
		return
	end

	dirt.full_repaint_required = true
	caret.autocompletion_popup = {
		buff = buff;
		selected_result = 1;
		results = {
			{ { colours.blue, "hello world" }, },
			{ { colours.white, "another " }, { colours.green, "one" } },
		};
	}
end

--- Render the autocompletion overlay.
-- @param caret	description
function render_autocompletion_popup( caret )
	local buff = caret.autocompletion_popup.buff
	local results = caret.autocompletion_popup.results

	local width = 15
	local height = min( #results, widget_view.height - 5 )

	local x = max( 0, min( widget_view.width - width - line_number_sidebar_width,
		buff:index_to_screen_space( caret[ 2 ], caret[ 1 ] ) + buff.scroll[ 1 ]
	) )
	local y = caret[ 2 ] - buff.scroll[ 2 ]
	x = x + line_number_sidebar_width

	if y + height > widget_view.height then
		y = y - height - 1
	end

	widget_view:draw_filled_rectangle( x, y, width, height, theme.secondary )

	for i = 1, #results do
		local result = results[ i ]
		if caret.autocompletion_popup.selected_result == i then
			widget_view
				:clear_line( y + i - 1, colours.light_grey, nil, nil, x, x + width - 1 )
				:write( x, y + i - 1, ">", colours.light_grey, theme.secondary )
		end

		render_fragments(
			widget_view,
			x + 1,
			y + i - 1,
			0,
			width - 1,
			caret.autocompletion_popup.selected_result == i and colours.light_grey or theme.secondary,
			result,
			caret.autocompletion_popup.selected_result == i and {
				[ colours.light_grey ] = theme.secondary
			} or nil
		)
	end
end

--- Render fragments to a Desktox buffer.
-- @param display_buffer	description
-- @param x	description
-- @param y	description
-- @param max_width	description
-- @param fragments	description
-- @return `display_buffer`
function render_fragments( display_buffer, x, y, x_offset, max_width, background, fragments, palette )
	local cursor = 0
	palette = palette or {}

	for _, fragment in ipairs( fragments ) do
		if cursor + #fragment[ 2 ] >= x_offset then
			display_buffer:write( x + cursor - x_offset, y,
				fragment[ 2 ]:sub( 1, max( 0, max_width - cursor + x_offset ) ),
				background,
				palette[ fragment[ 1 ] ] or fragment[ 1 ]
			)
		end

		cursor = cursor + #fragment[ 2 ]

		if cursor - x_offset > x + max_width then
			break
		end
	end

	return display_buffer
end

--- Find the first caret on a given line.
-- @param n	description
-- @return The index of the caret in `carets`, or nil if no carets were found
local function first_caret_on_line( buff, n )
	local low, high = 1, #buff.carets

	while low <= high do
		local middle = floor( ( high - low ) / 2 ) + low
		local y = buff.carets[ middle ][ 2 ]

		if y > n then
			high = middle - 1
		elseif y < n then
			low = middle + 1
		else	-- found a match
			repeat
				middle = middle - 1
			until not buff.carets[ middle ] or buff.carets[ middle ][ 2 ] ~= n
			return middle + 1
		end
	end

	return nil
end

--- Get all carets on a given line.
-- Uses binary search to speed things up.
-- @param n	description
-- @return An array of carets found
local function carets_on_line( buff, n )
	local result = {}

	local index = first_caret_on_line( buff, n )
	if index then
		while buff.carets[ index ] and buff.carets[ index ][ 2 ] == n do
			table.insert( result, buff.carets[ index ] )
			index = index + 1
		end
	end

	return result
end

--- Correct the tabs_scroll value.
-- @return nil
function fix_tabs_scroll()
	local tab_position = 1

	-- TODO pull this out into a function of its own, it's elsewhere too
	for i = 1, #open_buffers do
		if not open_buffers[ i ].hidden then
			open_buffers[ i ].displayed_title = open_buffers[ i ].displayed_title or "untitled"

			if open_buffers[ i ] == selected_buffer then
				break
			end

			tab_position = tab_position + #open_buffers[ i ].displayed_title + 3
		end
	end

	local title_len = 0

	if selected_buffer then
		title_len = selected_buffer.displayed_title
			and #selected_buffer.displayed_title
			or #"untitled"
	end

	tabs_scroll = max(
		-tab_position + 1,
		min(
			w - tab_position - title_len - 3, tabs_scroll
		)
	)
end

--- Show the widget for "Go To Anything".
-- @param init_query	description
-- @return nil
function show_go_to_anything( init_query )
	dirt.full_repaint_required = true

	if widget and widget.type == "go_to_anything" then
		is_view_disabled = false
		widget = nil

		return
	end

	is_view_disabled = true
	widget = {
		type = "go_to_anything";
		scroll = 0;
		query = init_query or "";
		-- TODO: dynamic/configurable
		width = floor( w * 0.6 );
		results = {};
	}

	widget.buffer = new_buffer(
		init_query,
		nil,
		widget.width,
		1,
		nil,
		false,
		true,
		true
	)

	widget.x = floor( w / 2 - widget.width / 2 )
	widget.listener = handler.new(
		buffer_view_listener.x1 + widget.x + screen.x1,
		buffer_view_listener.y1 + screen.y1,
		widget.width,
		buffer_view_listener.height
	):register_callback( "key", function( _, code )
		local handled = true

		if code == keys.backspace then
			refresh_widget()
		elseif held[ keys.leftCtrl ] and code == keys.j then
			widget.selected_result = max( 1, min(
				#widget.results[ widget.results.chain_position ], ( widget.selected_result or 1 ) + 1
			) )
			refresh_widget()
		elseif held[ keys.leftCtrl ] and code == keys.k then
			widget.selected_result = max( 1, min(
				#widget.results[ widget.results.chain_position ], ( widget.selected_result or 1 ) - 1
			) )
			refresh_widget()
		elseif code == keys.enter then
			local result = widget.results[ widget.results.chain_position ][ widget.selected_result or 1 ]

			if result and ( widget.selected_result ~= 1 or #widget.path_query > 0 or not widget.line ) then
				if not selected_buffer or selected_buffer.path ~= result.path then
					open_file(
						result.path,
						buffer_view.height,
						true,
						true,
						true
					)
				end

				open_buffers[ #open_buffers + 1 ] = selected_buffer

				if #selected_buffer.carets == 0 then
					selected_buffer:add_caret( 0, 1 )
				end

				schedule_lint( selected_buffer )
				schedule_highlight( selected_buffer )
			end

			if result or widget.line then
				dirt.full_repaint_required = true
				is_view_disabled = false
				widget = nil
			end
		elseif code == keys.down then
			widget.selected_result = max( 1, min(
				#widget.results[ widget.results.chain_position ], ( widget.selected_result or 1 ) + 1
			) )
			refresh_widget()
		elseif code == keys.up then
			widget.selected_result = max( 1, min(
				#widget.results[ widget.results.chain_position ], ( widget.selected_result or 1 ) - 1
			) )
			refresh_widget()
		else
			handled = false
		end

		if handled then
			dirt.full_repaint_required = true
		end
	end ):register_callback( "mouse_scroll", function( _, amount )
		widget.scroll = min( #widget.results * 2, max( 0, widget.scroll + amount * settings.vertical_scroll_speed ) )
	end )

	refresh_widget()

	widget.buffer.no_newline = true
	widget.buffer.hidden = true
end

--- Show the bytecode generated for tokens under a caret (currently the whole line).
-- @param buff	description
-- @param caret	description
-- @return nil
local function show_bytecode( buff, caret )
	local line = buff.lines[ caret[ 2 ] ]

	if not buff.fn or not line.instr then
		show_status "File not compiled"
		return
	end

	local header_size = 1
	table.insert( buff.lines, caret[ 2 ] + 1, {
		no_rehighlight = true;
		no_number = true;
		str = "";

		formatted = {
			{ colours.light_grey, " bytecode:" }
		}
	} )

	buff.n_lines = buff.n_lines + header_size
	local opcode_width = 10
	local format = "%3s"

	for i = 1, #line.instr do
		local instr = line.fn.code[ line.instr[ i ] ] or {}

		local opcode = lang.lua.opcodes[ instr.OP + 1 ] or "what"
		local formatted = {
			{ colours.cyan, " [" },
			{ colours.purple, tostring( line.instr[ i ] ) },
			{ colours.cyan, "] " },
			{ colours.cyan, opcode .. ( " " ):rep( opcode_width - #opcode ) },
			{ colours.purple, format:format( tostring( instr.A ) ) .. " " },
			{ colours.purple, format:format( tostring( instr.B or instr.Bx or instr.sBx ) ) .. " " },
		}

		if opcode == "JMP" then
			table.remove( formatted, 5 )
			formatted[ 5 ][ 2 ] = format:format( tostring( instr.Bx - 131071 ) )
		end

		if instr.C then
			table.insert(
				formatted,
				{ colours.purple, format:format( tostring( instr.C ) ) .. " " }
			)
		end

		local new_line = {
			no_rehighlight = true;
			no_number = true;
			str = "";

			formatted = formatted;
		}

		table.insert( buff.lines, caret[ 2 ] + i + header_size, new_line )
		buff.n_lines = buff.n_lines + 1
	end

	dirt.full_repaint_required = true
end

--- Reassign lint issues to buff.lines_with_issues.
-- @param buff	The buffer to operate on
-- @param start	The first dirty line
-- @param delta	The amount of lines added or removed
-- @return nil
local function reassign_issues( buff, start, delta )
	local copy = shallow_copy( buff.lines_with_issues or {} )

	for line_index, issues in pairs( copy ) do
		if line_index >= start then
			buff.lines_with_issues[ line_index + delta ] = issues
			buff.lines_with_issues[ line_index ] = nil

			for _, issue in pairs( issues ) do
				issue.line = issue.line + delta
			end
		end
	end
end

--- Invert lineinfo to add instruction indices to each line.
-- FIXME: memory-intensive. Complex relations between large objects
-- will be hard to garbage collect. We should remove this metadata
-- when possible.
-- @param buff	The buffer to operate on
-- @return nil
local function process_lineinfo( buff )
	for i = 1, buff.n_lines do
		buff.lines[ i ].p = nil
		buff.lines[ i ].fn = nil
		buff.lines[ i ].instr = nil
	end

	local fns = { buff.fn }
	local i = 1

	while i <= #fns do
		local fn = fns[ i ]

		if fn.lineDefined and fn.lineDefined > 0 and fn.lineDefined <= buff.n_lines then
			buff.lines[ fn.lineDefined ].p = buff.lines[ fn.lineDefined ].p or {}
			table.insert( buff.lines[ fn.lineDefined ].p, fn )
		end

		for j = 0, fn.p and #fn.p or -1 do
			table.insert( fns, fn.p[ j ] )
		end

		for j = 0, fn.lineinfo and #fn.lineinfo or -1 do
			buff.lines[ fn.lineinfo[ j ] ].fn = fn
			buff.lines[ fn.lineinfo[ j ] ].instr = buff.lines[ fn.lineinfo[ j ] ].instr or {}
			table.insert( buff.lines[ fn.lineinfo[ j ] ].instr, j )
		end

		i = i + 1
	end
end

--- Lint a buffer.
-- @param buff	description
-- @return nil
local function lint( buff )
	local file = ""

	for i = 1, buff.n_lines do
		file = file .. buff.lines[ i ].str .. "\n"
	end

	buff.lint_routine = coroutine.create( function()
		coroutine.yield(
			"result",
			luacheck.check_strings( { file }, env.computer_craft )
		)
	end )
end

--- Duh.
-- @return nil
local function process_lint_results( buff, results )
	buff.issues = results

	local lines_with_issues = {}
	buff.lines_with_issues = lines_with_issues

	local f = io.open( "/yelua/~compiled.lua", "w" )
	f:write( utils.deep_print( buff.issues ) )
	f:close()

	if buff.issues then
		for i = 1, #buff.issues do
			for j = 1, #buff.issues[ i ] do
				local issue = buff.issues[ i ][ j ]

				if issue.line > buff.n_lines then
					issue.line = buff.n_lines
					issue.column = #buff.lines[ buff.n_lines ].str + 1
					issue.end_column = issue.column
				end

				lines_with_issues[ issue.line ] = lines_with_issues[ issue.line ] or {}
				table.insert( lines_with_issues[ issue.line ], issue )
			end
		end
	end

	for _, local_issues in pairs( buff.lines_with_issues ) do
		table.sort( local_issues, errors_last_comparator )
	end

	process_lineinfo( buff )
end

--- Check that a line contains executable code.
-- @param buff	description
-- @param n	description
-- @return true if there are instructions emitted from this line
local function is_nonempty( buff, n )
	return ( buff.lines[ n ].instr and #buff.lines[ n ].instr > 0 )
		or ( buff.lines[ n ].p and #buff.lines[ n ].p > 0 )
		or ( buff.issues and buff.issues.code_lines and buff.issues.code_lines[ n ] )
end

--- Create a new buffer for editing.
-- @param str	description
-- @param title	description
-- @param stop_highlight_at	The line number to stop highlighting at
--                         	(if not set, will highlight the whole buffer)
-- @param no_caret	description
-- @param no_lint	description
-- @return The new buffer
function new_buffer( str, title, width, height, stop_hl_at, no_caret, no_lint, no_open )
	log "Creating new buffer" ( title, width, height )
	local buff = text_buffer.new( str, title, width, height )

	if not no_open then
		open_buffers[ #open_buffers + 1 ] = buff
	end

	if not no_caret then
		buff:add_caret( 0, 1 )
	end

	if not no_lint then
		schedule_lint( buff )
	end

	schedule_highlight( buff, 1, stop_hl_at )

	return buff
end

--- Change the selected buffer.
-- @param buff	description
-- @return buff
function change_buffer( buff )
	dirt.full_repaint_required = true
	selected_buffer = buff
	fix_tabs_scroll()
	return buff
end

--- Open a buffer for a file.
-- @param path	description
-- @return the new buffer
function open_file( path, ... )
	log "Opening" ( path )
	local f = io.open( path, "r" )
	local contents = f:read "*a"
	f:close()

	local buff = change_buffer(
		new_buffer(
			contents,
			path:match "^.-([^/]+)$",
			buffer_view.width,
			buffer_view.height,
			...
		)
	)

	buff.saved = true
	buff.path = path

	return buff
end

--- Close an open buffer.
-- @param buff	description
-- @return nil
local function close_buffer( buff )
	for i = 1, #open_buffers do
		if open_buffers[ i ] == buff then
			dirt.full_repaint_required = true
			table.remove( open_buffers, i )

			if buff == selected_buffer then
				change_buffer( open_buffers[ min( i, #open_buffers ) ] )
			end

			fix_tabs_scroll()

			return
		end
	end
end

--- Show a status message in the status bar.
-- @param message	description
-- @return nil
function show_status( message )
	table.insert( statuses, { msg = message; timer = os.startTimer( settings.status_display_duration ) } )
end

--- Insert text.
-- @param caret	description
-- @param data	description
-- @return edit
function edit.insert( caret, data )
	local buff = widget and widget.buffer or selected_buffer

	if caret.selection then
		-- TODO
	end

	local first_newline = data:find( "\n" ) or 0

	local first_dirty = caret[ 2 ]
	local line = buff.lines[ caret[ 2 ] ]
	local str = line.str
	local org_x = caret[ 1 ]
	local lines_created = 0

	line.str = str:sub( 1, caret[ 1 ] ) .. data:sub( 1, first_newline - 1 )

	if first_newline > 0 then
		for new_line in data:gmatch "\n([^\n]*)" do
			lines_created = lines_created + 1
			table.insert( buff.lines, caret[ 2 ] + lines_created, {
				str = new_line;
			} )
		end

		local index = first_caret_on_line( buff, caret[ 2 ] )
		if index then
			for i = index, #buff.carets do
				buff.carets[ i ][ 2 ] = buff.carets[ i ][ 2 ] + lines_created
			end
		end

		buff.n_lines = buff.n_lines + lines_created
	end

	local split = caret[ 1 ] + 1
	caret[ 1 ] = #buff.lines[ caret[ 2 ] ].str
	caret.desired_x = buff:index_to_screen_space( caret[ 2 ], caret[ 1 ] - 1 ) + 1

	for _, other_caret in ipairs( carets_on_line( buff, caret[ 2 ] ) ) do
		if other_caret[ 1 ] > caret[ 1 ] then
			other_caret[ 1 ] = other_caret[ 1 ] + caret[ 1 ] - org_x
			other_caret.desired_x = buff:index_to_screen_space( other_caret[ 2 ], other_caret[ 1 ] - 1 ) + 1
		end
	end

	-- TODO: perhaps in other cases than #data == 1 as well?
	local balance = edit.auto_balance[ data ] or ""

	-- avoid re-inserting balancing characters (such as the second parenthesis)
	if #data ~= 1
	or edit.auto_balance_inv[ data ] ~= str:sub( caret[ 1 ] - 1, caret[ 1 ] - 1 ) then
		buff.lines[ caret[ 2 ] ].str = buff.lines[ caret[ 2 ] ].str .. balance .. str:sub( split, -1 )
	end

	schedule_highlight( buff, max( 1, first_dirty - 1 ) )

	dirt.full_repaint_required = true
	buff.saved = false
	buff.dirty = true

	reassign_issues( buff, first_dirty, lines_created )
	buff:scroll_to_caret( caret )
	fix_tabs_scroll()

	return edit
end

--- Remove text.
-- @param caret	description
-- @param n	description
-- @param before	description
-- @return edit
function edit.remove( caret, n, before )
	local buff = widget and widget.buffer or selected_buffer

	if caret.selection then
		-- TODO
	end

	local line = buff.lines[ caret[ 2 ] ]
	local lines_removed = 0

	if before then
		while n > 0 do
			if n <= caret[ 1 ] then
				-- The easy case, no need to join lines
				local curr, nxt = line.str:sub( caret[ 1 ], caret[ 1 ] ), line.str:sub( caret[ 1 ] + 1, caret[ 1 ] + 1 )

				local leftover = line.str:sub( caret[ 1 ] + 1, -1 )
				if nxt == edit.auto_balance[ curr ] then
					leftover = line.str:sub( caret[ 1 ] + 2, -1 )
				end

				line.str =  line.str:sub( 1, caret[ 1 ] - n )
				         .. leftover
				caret[ 1 ] = caret[ 1 ] - n
				n = 0
			else
				n = n - caret[ 1 ] - 1 -- An extra character - the newline
				local leftover = line.str:sub(
					-- Index from 1
					caret[ 1 ] + 1,
					-1
				)

				if caret[ 2 ] == 1 then
					caret[ 1 ] = 0
					break
				end

				table.remove( buff.lines, caret[ 2 ] )
				buff.n_lines = buff.n_lines - 1
				lines_removed = lines_removed + 1

				caret[ 2 ] = caret[ 2 ] - 1
				line = buff.lines[ caret[ 2 ] ]
				caret[ 1 ] = #line.str
				line.str = line.str .. leftover
			end
		end
	elseif caret[ 2 ] ~= buff.n_lines or caret[ 1 ] ~= #buff.lines[ caret[ 2 ] ].str then
		-- Deleting means moving right and backspacing
		motion.right( caret, n, false )
		edit.remove( caret, n, true )
	end

	schedule_highlight( buff, caret[ 2 ] )

	dirt.full_repaint_required = true
	buff.saved = false
	buff.dirty = true

	reassign_issues( buff, caret[ 2 ], -lines_removed )
	buff:scroll_to_caret( caret )
	fix_tabs_scroll()

	return edit
end

--- Draw the go_to_anything widget.
-- @return nil
local function draw_go_to_anything()
	local line = widget.buffer.lines[ 1 ].str:sub(
		-widget.buffer.scroll[ 1 ] + 1,
		-widget.buffer.scroll[ 1 ] + widget.width + 1
	)

	log "drawing go_to_anything" ( line )

	for i = floor( max( 1, widget.scroll / 2 + 1 ) ), #widget.results[ widget.results.chain_position ] do
		if 1 + i * 2 - widget.scroll > widget_view.height then break end
		local result = widget.results[ widget.results.chain_position ][ i ]

		widget_view:draw_filled_rectangle(
			widget.x,
			i * 2 - widget.scroll,
			widget.width,
			2,
			widget.selected_result == i and colours.light_grey or theme.secondary
		)

		if i == widget.selected_result then
			widget_view:write( widget.x, i * 2 - widget.scroll, "\187", colours.light_grey, theme.secondary )
		end

		render_fragments(
			widget_view,
			widget.x + 1,
			i * 2 - widget.scroll,
			0,
			widget.width - 2,
			widget.selected_result == i and colours.light_grey or theme.secondary,
			result.formatted_filename,
			widget.selected_result == i and {
				[ colours.light_grey ] = theme.secondary
			} or nil
		)

		render_fragments(
			widget_view,
			widget.x + 1,
			i * 2 + 1 - widget.scroll,
			0,
			widget.width - 2,
			widget.selected_result == i and colours.light_grey or theme.secondary,
			result.formatted,
			widget.selected_result == i and {
				[ colours.light_grey ] = theme.secondary
			} or nil
		)
	end

	widget_view
		:draw_filled_rectangle( widget.x, 0,
			widget.width,
			2,
			theme.secondary
		):write( widget.x, 0,
			line,
			theme.secondary,
			theme.foreground
		):clear_line( 1,
			theme.accent,
			theme.secondary,
			"\143",
			widget.x,
			widget.x + widget.width - 1
		)

	local str = widget.buffer.lines[ 1 ].str
	local hint

	if #str == 0 then
		hint = "Go to anything.."
	elseif str:sub( -1, -1 ) == ":" then
		hint = "line.."
	elseif str:sub( -1, -1 ) == "@" then
		hint = "symbol.."
	elseif str:sub( -1, -1 ) == "#" then
		hint = "term.."
	elseif str:sub( -1, -1 ) == "!" then
		hint = "error.."
	end

	if hint then
		widget_view:write( widget.x + #str, 0, hint, theme.secondary, colours.light_grey )
	end

	if selected_buffer and type( widget.line ) == "number" then
		if widget.line < 0 then
			-- -1 is the last line of the buffer
			widget.line = selected_buffer.n_lines + widget.line + 1
		end

		-- TODO: backup in case go to anything is cancelled
		selected_buffer:reset_carets()
		selected_buffer:add_caret( 0, 1 )
		selected_buffer.carets[ 1 ].motion.jump.to( widget.line ).line()
	end
end

--- Get an array of all files under a directory.
-- @param path	description
-- @return The full paths of the results
local function get_all_files( path )
	local directories = { path }
	local files = {}

	while #directories > 0 do
		local dir = table.remove( directories )
		dir = dir:match "^(.-)/*$"
		-- FIXME: implement for lfs as well
		for _, file in pairs( fs.find( dir .. "/*" ) ) do
			if not file:match "^.-/%.[^/]*$" then
				if fs.isDir( file ) then
					table.insert( directories, file )
				else
					if file:sub( 1, 1 ) ~= "/" then
						file = "/" .. file
					end

					table.insert( files, file )
				end
			end
		end
	end

	-- TODO we don't want just the names, actually
	return files
end

--- Reindex the filesystem (why are all function names so self-explanatory?).
-- @return nil
local function reindex_filesystem()
	log "Reindexing filesystem"
	filesystem.files = get_all_files "/"
	log( "Found " .. #filesystem.files .. " files total" )
end

--- Fuzzily match against an array of tostring()able values.
-- @param query	description
-- @param data	description
-- @return An array of matches
local function fuzzy_filter( query, data )
	local results = {}

	-- TODO: validate the pattern and try using it
	-- TODO remove this for patterns
	query = query:lower()
	local query_len = #query

	local string_match = string.match
	local lower = string.lower
	local sub = string.sub

	for _, entry in ipairs( data ) do
		entry = tostring( entry[ 1 ] or entry )
		local normalised_entry = lower( entry )

		local entry_len = #entry
		local n_slices = 0
		local slices = {}
		local match = query == ""
		local score = -entry_len * 2

		local i = 1
		local j = 1

		while i <= query_len do
			local start_j = j

			while i <= query_len
			and   j <= entry_len
			and sub( query, i, i ) == sub( normalised_entry, j, j ) do
				i = i + 1
				j = j + 1
			end

			n_slices = n_slices + 1
			slices[ n_slices ] = sub( entry, start_j, j - 1 )

			if j ~= start_j then
				if start_j == 1
				or (
					start_j ~= 1
					and string_match( sub( normalised_entry, start_j - 1, start_j - 1 ), "^%W$" )
				) then
					score = score + 100

					if j - 1 == entry_len
					or (
						j - 1 ~= entry_len
						and string_match( sub( normalised_entry, j, j ), "^%W$" )
					) then
						score = score + 70
					end
				end
			end

			score = score + 4 ^ ( j - start_j ) - 1.5 ^ start_j

			if i > query_len then
				-- end of query (match)

				n_slices = n_slices + 1
				slices[ n_slices ] = sub( entry, j, -1 )

				match = true
				break
			end

			start_j = j
			while j <= entry_len
			and sub( query, i, i ) ~= sub( normalised_entry, j, j ) do
				j = j + 1
			end

			if j == start_j then
				-- end of entry (no match)
				break
			end

			n_slices = n_slices + 1
			slices[ n_slices ] = sub( entry, start_j, j - 1 )
		end

		if match then
			table.insert( results, { entry, slices, score } )
		end
	end

	return results
end

--- Get the issue under a caret.
-- @param buff	description
-- @param caret	description
-- @return The issue or nil, if none is found
local function get_issue_under_caret( buff, caret )
	local local_issues = buff.lines_with_issues[ caret[ 2 ] ]

	if local_issues then
		for _, issue in ipairs( local_issues ) do
			if caret[ 1 ] >= issue.column - 1 and caret[ 1 ] <= issue.end_column then
				return issue
			end
		end
	end
end

--- Refresh the currently active widget.
-- @return nil
function refresh_widget()
	if widget.type == "go_to_anything" then
		widget.line = nil
		widget.scroll = 0

		widget.selected_result = max( 1, min(
			#( widget.results[ widget.results.chain_position ] or "" ),
			widget.selected_result or 1
		) )

		local query = widget.buffer.lines[ 1 ].str:lower()
		local files

		if widget.last_query and widget.last_query == query:sub( 1, -2 ) then
			files = widget.previous_results
		else
			selected_buffer = widget.previous_buffer or selected_buffer
			files = fuzzy_filter( "", filesystem.files )
		end

		local path_query, symbol_query, fuzzy_query, error_query, line_query
			= query:match "^([^:@#]*)(@?.-)(#?.-)(%!?.-)(:?%-?%d*)$"

		files = fuzzy_filter( path_query:gsub( "%s+", "" ), files )

		local line
		if line_query then
			line = line_query:match "^:(%-?%d+)$"
		end

		local symbol
		if symbol_query then
			symbol = symbol_query:match "^@(.+)$"
		end

		local fuzzy
		if fuzzy_query then
			fuzzy = fuzzy_query:match "^#(.+)$"
		end

		local err
		if error_query then
			err = error_query:match "^%!(.+)$"
		end

		widget.path_query = path_query
		widget.line = tonumber( line )
		widget.results = {
			chain_position = 1;
			{}
		}

		table.sort( files, function( this, that )
			if this[ 3 ] == that[ 3 ] then
				return this[ 1 ] < that[ 1 ]
			end

			return this[ 3 ] > that[ 3 ]
		end )

		-- TODO: this kind of matching is pretty expensive,
		-- do it lazily and eval more on scroll
		for _, file in ipairs( files ) do
			local formatted_filename = {}
			local formatted = {}

			if #file[ 2 ] == 0 then
				table.insert( formatted_filename, {
					theme.foreground,
					file[ 1 ]:match "[^/]*$"
				} )

				table.insert( formatted, {
					colours.light_grey,
					file[ 1 ]
				} )
			else
				local match = true
				for i = 1, #file[ 2 ] do
					table.insert( formatted_filename, {
						match and theme.primary or theme.foreground,
						file[ 2 ][ i ]
					} )
					table.insert( formatted, {
						match and theme.primary or colours.light_grey,
						file[ 2 ][ i ]
					} )

					match = not match
				end

				local index = #file[ 1 ]
				local last_slash = file[ 1 ]:match "()[^/]*$"
				local formatted_filename_len = #formatted_filename

				for i = formatted_filename_len, 1, -1 do
					local fragment_len = #formatted_filename[ i ][ 2 ]

					if index < last_slash then
						table.remove( formatted_filename, i )
					elseif index - fragment_len < last_slash then
						-- split the current fragment
						formatted_filename[ i ] = {
							formatted_filename[ i ][ 1 ],
							formatted_filename[ i ][ 2 ]:sub( fragment_len - index + last_slash, -1 )
						}
					end

					index = index - fragment_len
				end
			end

			table.insert( widget.results[ widget.results.chain_position ], {
				path = file[ 1 ];
				err = err;
				line = line;
				fuzzy = fuzzy;
				score = file[ 3 ];
				symbol = symbol;
				formatted = formatted;
				formatted_filename = formatted_filename;
			} )
		end


		if #widget.results[ widget.results.chain_position ] > 0 then
			if widget.selected_result then
				widget.selected_result = max( 1, min(
					#widget.results[ widget.results.chain_position ], widget.selected_result
				) )
			end

			if ( #widget.path_query > 0 or widget.selected_result ~= 1 ) and (
				not selected_buffer
				or selected_buffer.path ~= widget.results[ widget.results.chain_position ][ widget.selected_result ].path
			) then
				if not widget.previous_buffer then
					widget.previous_buffer = selected_buffer
				end

				selected_buffer = open_file(
					widget.results[ widget.results.chain_position ][ widget.selected_result ].path,
					buffer_view.height,
					true,
					true,
					true
				)
			end
		end

		widget.last_query = query
		widget.previous_results = files
	end
end

--- Draw the widget.
-- @return nil
local function draw_widget()
	widget_view:clear( colours.transparent_bg, colours.transparent_fg, "\0" )

	if widget then
		if widget.type == "go_to_anything" then
			draw_go_to_anything()
		end
	end
end

--- Draw the buffer view.
-- @return nil
local function draw_buffer_view()
	buffer_view:clear( theme.background )

	longest_line_width = 0
	local lines_with_carets = {}

	-- FIXME sort out that thing with vanilla_colours
	local caret_highlight = colours.purple

	local warning_palette = {
		[ theme.warn ] = theme.background
	}
	local error_palette = {
		[ theme.err ] = theme.background;
	}

	local highlighted_line_background = {
		[ theme.secondary ] = colours.light_grey;
	}

	local scroll = selected_buffer.scroll
	local carets = selected_buffer.carets
	for i = 1, #carets do
		lines_with_carets[ carets[ i ][ 2 ] ] = lines_with_carets[ carets[ i ][ 2 ] ] or {}
		table.insert( lines_with_carets[ carets[ i ][ 2 ] ], carets[ i ][ 1 ] )
	end

	-- FIXME this NEEDS to be handled differently
	local line_number_offset = 0
	for i = 1, scroll[ 2 ] do
		if selected_buffer.lines[ i ].no_number then
			line_number_offset = line_number_offset - 1
		end
	end

	local numberless_lines_in_view = 0
	for i = 1 + scroll[ 2 ], min( selected_buffer.n_lines, buffer_view.height + scroll[ 2 ] ) do
		if selected_buffer.lines[ i ].no_number then
			numberless_lines_in_view = numberless_lines_in_view + 1
		end
	end

	line_number_sidebar_width = math.ceil(
		math.log10(
			selected_buffer.n_lines
			+ 1
			+ line_number_offset
			- numberless_lines_in_view
		)
	) + 2

	selected_buffer.width = buffer_view.width - line_number_sidebar_width

	-- this is just getting uglier and uglier
	local cummulative_no_number_offset = 0
	for i = 1 + scroll[ 2 ], buffer_view.height + scroll[ 2 ] do
		local caret_positions = lines_with_carets[ i ]
		local highlighted = ( not is_view_disabled and caret_positions )
			and settings.highlight_line_with_caret

		buffer_view
			-- line with a caret should be highlighted
			:clear_line( i - scroll[ 2 ] - 1, highlighted and theme.secondary or theme.background )

		if i >= 1 and i <= selected_buffer.n_lines then
			if selected_buffer.lines[ i ].no_number then
				cummulative_no_number_offset = cummulative_no_number_offset + 1
			end

			-- actual buffer contents
			longest_line_width = max( longest_line_width, #selected_buffer.lines[ i ].str )
			if selected_buffer.lines[ i ].formatted then
				render_fragments(
					buffer_view,
					line_number_sidebar_width,
					i - scroll[ 2 ] - 1,
					-scroll[ 1 ],
					buffer_view.width - line_number_sidebar_width,
					highlighted and theme.secondary or theme.background,
					selected_buffer.lines[ i ].formatted,
					highlighted and highlighted_line_background or nil
				)
			else
				buffer_view:write( line_number_sidebar_width, i - scroll[ 2 ] - 1,
					selected_buffer.lines[ i ].str,
					highlighted and theme.secondary or theme.background,
					theme.foreground
				)
			end

			if not selected_buffer.lines[ i ].no_number then
				-- line number sidebar
				local line_number_label = ( "%" .. line_number_sidebar_width - 2 .. "u" )
					:format( i + line_number_offset - cummulative_no_number_offset ) .. " "

				buffer_view
					:write( 0, i - scroll[ 2 ] - 1,
						line_number_label,
						highlighted and theme.secondary or theme.background,
						( caret_positions and settings.highlight_line_number_with_caret )
							and (
								( is_nonempty( selected_buffer, i ) or is_view_disabled )
									and theme.accent
									or ( settings.highlight_line_with_caret
										and theme.background
										or theme.secondary
									)
							) or (
								is_nonempty( selected_buffer, i )
								and theme.code_line
								or theme.secondary
							)
					)
			end

			local issues = selected_buffer.issues
				and selected_buffer.lines_with_issues[ i ]
				or nil

			if issues and #issues > 0 then
				local has_error = false

				for j = #issues, 1, -1 do
					local issue = issues[ j ]
					local is_error = issue.code:sub( 1, 1 ) == "0"
					has_error = has_error or is_error

					local x1 = max(
						line_number_sidebar_width,
						line_number_sidebar_width
						+ scroll[ 1 ]
						+ selected_buffer:index_to_screen_space( i, issue.column - 1 )
					)

					local x2 = line_number_sidebar_width
						+ scroll[ 1 ]
						+ selected_buffer:index_to_screen_space( i, issue.end_column )
						- 1

					local y = max( 0, min( buffer_view.height - 1,
						i - scroll[ 2 ] - 1 + cummulative_no_number_offset
					) )

					if x1 <= x2 and x1 < buffer_view.width then
						buffer_view:draw_filled_rectangle_from_points_v_dtt(
							min( buffer_view.width - 1, x1 ),
							y,
							min( buffer_view.width - 1, x2 ),
							y,
							is_error and theme.err or theme.warn,
							theme.background,
							"\0"
						)
					end
				end

				buffer_view:write(
					line_number_sidebar_width - 2,
					i - scroll[ 2 ] - 1,
					has_error
						and "x"
						or ( #issues > 1 and "\19" or "!" ),
					highlighted and theme.secondary or theme.background,
					has_error and theme.err or theme.warn
				)
			end

			if caret_positions and ( #carets > 1 or settings.force_block_caret ) then
				for _, pos in pairs( caret_positions ) do
					local x = line_number_sidebar_width + scroll[ 1 ] + selected_buffer:index_to_screen_space( i, pos )

					if x >= line_number_sidebar_width and x < buffer_view.width then
						buffer_view:draw_filled_rectangle_from_points_v_dtt(
							x,
							i - scroll[ 2 ] - 1,
							x,
							i - scroll[ 2 ] - 1,
							caret_highlight,
							theme.background,
							"\0"
						)
					end
				end
			end
		end

		buffer_view
			-- TODO: change to clear_column()
			:write( line_number_sidebar_width - 1, i - scroll[ 2 ] - 1,
				i == scroll[ 2 ] + 1
					and ( "\148" )
					or ( i == buffer_view.height + scroll[ 2 ] + screen.y1 + buffer_view.y1 - 1
						and "\133"
						or "\149"
					),
				highlighted and theme.secondary  or theme.background,
				caret_positions and theme.accent or theme.secondary
			)
	end

	buffer_view:render()
end

--- Draw the tabs bar.
-- @return nil
local function draw_tabs_view()
	if #open_buffers == 0 then
		tabs_view:clear( theme.background )
		return
	end

	-- TODO: configurable
	tabs_view:clear( colours.light_grey, theme.foreground, " " )

	local cursor = 1 + tabs_scroll

	local total_width = 0
	for _, buff in pairs( open_buffers ) do
		if not buff.hidden then
			local title = buff.title or "untitled"

			if not buff.title and buff.n_lines > 0 and buff.lines[ 1 ].str:find( "%S" ) then
				title = buff.lines[ 1 ].str
			end

			buff.displayed_title = title
			total_width = total_width + #title + 3
		end
	end

	for _, buff in ipairs( open_buffers ) do
		if not buff.hidden then
			local title = buff.displayed_title

			if #title > settings.max_tab_title_length then
				title = title:sub( 1, settings.max_tab_title_length - 2 ) .. ".."
			end

			buff.displayed_title = title

			if buff == selected_buffer then
				fix_tabs_scroll()
			end

			local tab_background
			if buff == selected_buffer then
				if settings.highlight_line_with_caret
				and not is_view_disabled
				and first_caret_on_line( selected_buffer, selected_buffer.scroll[ 2 ] + 1 ) then
					tab_background = theme.secondary
				else
					tab_background = theme.background
				end
			else
				tab_background = theme.foreground
			end

			tabs_view:write( cursor, 0,
				title .. " ",
				tab_background,
				buff == selected_buffer
					and theme.foreground
					or theme.background
			)

			cursor = cursor + #title + 1

			tabs_view:write( cursor, 0,
				buff.saved and "x" or "\7",
				tab_background,
				colours.light_grey
			)

			cursor = cursor + 2
		end
	end
end

--- Draw the screen shown with no open buffers.
-- @return nil
local function draw_welcome_screen()
	buffer_view:clear( theme.background )

	local key_highlights = setmetatable( {
		Ctrl  = theme.accent;
		Shift = theme.primary;
	}, {
		__index = function( t, k )
			local names = {}

			for name, code in pairs( colours ) do
				if not name:lower():find "transparent"
				and code ~= theme.background
				and code ~= theme.secondary then
					names[ #names + 1 ] = name
				end
			end

			t[ k ] = colours[ names[ ( k:byte() + 10 ) % #names + 1 ] ]
			return t[ k ]
		end
	} )

	local message = [[
      Ctrl+P  go to a file
      Ctrl+S  save
      Ctrl+Q  quit
      Ctrl+F  search
      Ctrl+W  close a tab
      Ctrl+B  show bytecode for
              the current line

    Ctrl+LMB  add a caret
    Ctrl+RMB  remove a caret

Ctrl+Shift+R  run
Ctrl+Shift+I  reindent
Ctrl+Shift+E  go to the next error
Ctrl+Shift+F  search everywhere
Ctrl+Shift+P  see all commands
]]
	local n_lines = 0
	local longest_line = 0
	for line in message:gmatch "(.-)\n" do
		if #line > longest_line then
			longest_line = #line
		end

		local meta, combo, meaning = line:match "^(%s*%a+)(%+[%+%a]+)(%s+.+)$"
		if meta then
			local cursor = 0
			buffer_view:write( 0, n_lines,
				meta,
				theme.background,
				key_highlights[ meta:gsub( "^%s+", "" ) ]
			)

			cursor = cursor + #meta

			for key in combo:gmatch "%+(%a+)" do
				local fg, bg = key_highlights[ key ], theme.background

				buffer_view
					:write( cursor, n_lines, "+", theme.background, theme.secondary )
					:write( cursor + 1, n_lines, key, bg, fg )
				cursor = cursor + #key + 1
			end

			buffer_view:write( cursor, n_lines, meaning, theme.background, colours.light_grey )
		else
			buffer_view:write( 0, n_lines, line, theme.background, colours.light_grey )
		end

		n_lines = n_lines + 1
	end

	buffer_view:shift(
		-floor( w / 2 - longest_line / 2 ),
		-floor( h / 2 - n_lines / 2 - 1 ),
		theme.background
	):render()
end

--- Redraw the status bar (duh).
-- @return nil
local function redraw_status_bar()
	local caret_pos = ""

	local carets
	local bar_fragments = {}

	if selected_buffer then
		carets = selected_buffer.carets

		if selected_buffer.issues then
			local warns = selected_buffer.issues.warnings
			local errs = selected_buffer.issues.errors

			local warn_frag = {
				warns == 0
					and ( errs == 0 and colours.green or theme.foreground )
					or theme.warn,
				warns .. " warn" .. ( warns == 1 and "" or "s" )
			}
			local separator_frag = {
				theme.foreground, ", "
			}
			local err_frag = {
				errs == 0
					and ( warns == 0 and colours.green or theme.foreground )
					or theme.err,
				errs .. " err" .. ( errs == 1 and "" or "s" )
			}

			if errs > 0 then
				table.insert( bar_fragments, err_frag )
				table.insert( bar_fragments, separator_frag )
				table.insert( bar_fragments, warn_frag )
			else
				table.insert( bar_fragments, warn_frag )
				table.insert( bar_fragments, separator_frag )
				table.insert( bar_fragments, err_frag )
			end

			table.insert( bar_fragments, {
				theme.foreground, " "
			} )
		end

		for i = #statuses, 1, -1 do
			table.insert( bar_fragments, 1, {
				theme.foreground, statuses[ i ].msg .. "; "
			} )
		end

		if #carets == 0 then
			caret_pos = "no carets"
		elseif #carets == 1 then
			caret_pos = carets[ 1 ][ 2 ].. ":" .. selected_buffer:index_to_screen_space(
				carets[ 1 ][ 2 ], carets[ 1 ][ 1 ]
			) + 1
		else
			caret_pos = #carets .. " carets"
		end

		if selected_buffer.issues and #carets > 0 then
			local issue = get_issue_under_caret( selected_buffer, carets[ 1 ] )
			if issue then
				bar_fragments = { {
					issue.code:sub( 1, 1 ) == "0" and theme.err or theme.warn,
					luacheck.get_message( issue ) .. " "
				} }
			end

		elseif not selected_buffer.no_lint then
			table.insert( bar_fragments, {
				theme.secondary,
				selected_buffer.lint_state or "Processing.."
			} )
		end
	end

	if debug and tostring( debug ) ~= "" then
		table.insert( bar_fragments, 1, { colours.light_blue, tostring( debug ).. " " } )
	end

	status_bar_buffer:clear( theme.background )

	if selected_buffer then
		status_bar_buffer
			:write( w - #caret_pos, 0,
				caret_pos,
				theme.background,
				theme.accent
			)
			:write( w - #caret_pos - 4, 0,
				selected_buffer.mode == "insert" and "INS" or "REP",
				theme.background,
				theme.foreground
			)
	end

	render_fragments(
		status_bar_buffer,
		0, 0, 0,
		w - #caret_pos - 5,
		theme.background,
		bar_fragments
	)
end

--- I mean, it's obvious <shrug>.
local function draw_autocompletion_popups()
	local carets = widget
		and widget.buffer.carets
		or selected_buffer and selected_buffer.carets

	if not carets then
		return
	end

	for i = 1, #carets do
		local caret = carets[ i ]

		if caret.autocompletion_popup then
			render_autocompletion_popup( caret )
		end
	end
end

--- Draw the carets.
-- @return nil
local function draw_carets()
	local carets = widget
		and widget.buffer.carets
		or selected_buffer and selected_buffer.carets

	if carets then
		if #carets == 1 and ( widget or not settings.block_caret ) then
			-- FIXME: needs offsetting for scroll and buffer view position
			local x = 1 + buffer_view.x1

			if widget then
				x = x + widget.x
				      + widget.buffer:index_to_screen_space( carets[ 1 ][ 2 ], carets[ 1 ][ 1 ] )
			else
				x = x + selected_buffer.scroll[ 1 ]
				      + line_number_sidebar_width
				      + selected_buffer:index_to_screen_space( carets[ 1 ][ 2 ], carets[ 1 ][ 1 ] )
			end

			local y = carets[ 1 ][ 2 ]
				+ screen.y1
				+ buffer_view.y1
				- ( widget and 0 or selected_buffer.scroll[ 2 ] )

			if not settings.force_block_caret
			and x >= buffer_view.x1 + line_number_sidebar_width + 1
			and x <= w
			and y > buffer_view.y1 + screen.y1
			and y <= buffer_view.y2 + screen.y2 + 1 then
				main_win.setCursorPos( x, y )
				main_win.setTextColour( theme.caret )
				main_win.setCursorBlink( true )
			else
				main_win.setCursorBlink( false )
			end
		end
	end
end

--- Redraw the GUI.
-- @return nil
local function redraw()
	if selected_buffer
	and selected_buffer.dirty
	and not selected_buffer.no_lint then
		schedule_lint( selected_buffer )
		selected_buffer.dirty = false
	end

	if dirt.full_repaint_required then
		screen:clear( theme.background )
		draw_widget()

		if selected_buffer then
			draw_buffer_view()
		else
			draw_welcome_screen()
		end
	else
		-- TODO: Only redraw affected lines
	end

	if dirt.full_repaint_required then
		if main_win.setVisible then
			main_win.setVisible( false )
		end

		main_win.setCursorBlink( false )
		draw_autocompletion_popups()
		widget_view:render()
		screen:render_to_window( main_win, 1, 2 )
	end

	if dirt.full_repaint_required or dirt.status_bar_repaint_required then
		if main_win.setVisible then
			main_win.setVisible( false )
		end

		redraw_status_bar()
		status_bar_buffer:render_to_window( main_win, 1, h )
	end

	if dirt.full_repaint_required or dirt.tabs_repaint_required then
		if main_win.setVisible then
			main_win.setVisible( false )
		end

		draw_tabs_view()
		tabs_view:render_to_window( main_win, 1, 1 )
	end

	draw_carets()

	if dirt.full_repaint_required
	or dirt.tabs_repaint_required
	or dirt.status_bar_repaint_required then
		-- TODO: support multishell
		if main_win.setVisible then
			main_win.setVisible( true )
		end
	end

	dirt.full_repaint_required = false
	dirt.tabs_repaint_required = false
	dirt.status_bar_repaint_required = false
end

local last_click
local running = true

tabs_view_listener
	:register_callback( "mouse_click", function( _, _, x, _ )
		x = x + tabs_scroll

		for _, buff in pairs( open_buffers ) do
			if buff.tab and buff.tab.x1 < x and buff.tab.x2 > x then
				change_buffer( buff )
				return
			end
		end
	end )
	:register_callback( "mouse_scroll", function( _, amount )
		dirt.tabs_repaint_required = true
		dirt.status_bar_repaint_required = true
		tabs_scroll = tabs_scroll - amount * settings.horizontal_scroll_speed
		debug = tabs_scroll
	end )

buffer_view_listener
	:register_callback( "timer", function( _, id )
		if id == reindex_timer then
			reindex_timer = os.startTimer( settings.filesystem_reindex_interval )
			reindex_filesystem()

			return
		end

		for i = 1, #statuses do
			if id == statuses[ i ].timer then
				dirt.status_bar_repaint_required = true
				table.remove( statuses, i )
				return
			end
		end

		for buff, timer in pairs( scheduled_lints ) do
			if id == timer then
				scheduled_lints[ buff ] = nil
				dirt.full_repaint_required = true
				lint( buff )

				return
			end
		end
	end )
	:register_callback( "mouse_click", function( _, button, x, y )
		if widget then
			widget.listener:handle_and_return( "mouse_click", button, x, y )
		elseif selected_buffer and button == 1 then
			y = max( 1, min( selected_buffer.n_lines, y + selected_buffer.scroll[ 2 ] - screen.y1 - buffer_view.y1 ) )
			x = max( 0, min(
				selected_buffer:index_to_screen_space( y, #selected_buffer.lines[ y ].str ),
				x - selected_buffer.scroll[ 1 ] - line_number_sidebar_width - 1 - buffer_view.x1
			) )

			last_click = { x, y }

			if not held[ keys.leftCtrl ] and not held[ keys.rightCtrl ] then
				selected_buffer:reset_carets()
			end

			selected_buffer:scroll_to_caret(
				selected_buffer:add_caret(
					selected_buffer:screen_space_to_index( y, x ),
					y
				)
			)
		end
	end )
	:register_callback( "mouse_scroll", function( _, amount, x, y )
		local shift_down = held[ keys.leftShift ] or held[ keys.rightShift ]
		amount = amount * (
			shift_down and settings.horizontal_scroll_speed or settings.vertical_scroll_speed
		)

		if widget then
			widget.listener:handle_and_return( "mouse_scroll", amount, x, y )
		end

		if shift_down then
			selected_buffer.scroll[ 1 ] = min( 0, max( -longest_line_width + 1, selected_buffer.scroll[ 1 ] - amount ) )
		elseif not widget then
			selected_buffer.scroll[ 2 ] = max( 0, min( selected_buffer.n_lines - 1, selected_buffer.scroll[ 2 ] + amount ) )
		end

		dirt.full_repaint_required = true
	end )
	:register_callback( "paste", function( _, text )
		-- FIXME fix paste for widgets
		local _, n_newlines = text:gsub( "\n", "" )

		if n_newlines > 0 and n_newlines + 1 == #selected_buffer.carets then
			local n = 0
			local pasted_lines = {}
			for line in ( text .. "\n" ):gmatch "(.-)\n" do
				n = n + 1
				pasted_lines[ n ] = line
			end

			for i, caret in ipairs( selected_buffer.carets ) do
				edit[ selected_buffer.mode ]( caret, pasted_lines[ i ] )
			end
		else
			-- Normal paste
			for _, caret in pairs( selected_buffer.carets ) do
				edit[ selected_buffer.mode ]( caret, text )
			end
		end
	end )
	:register_callback( "char", function( _, char )
		local buff = widget and widget.buffer or selected_buffer

		if not buff or held[ keys.leftCtrl ] then
			return
		end

		for _, caret in pairs( buff.carets ) do
			-- TODO rework this
			edit[ buff.mode ]( caret, char )
		end

		if widget then
			refresh_widget()
		end

		dirt.full_repaint_required = true
	end )
	:register_callback( "key", function( _, code )
		held[ code ] = true

		if code == keys.q and held[ keys.leftCtrl ] then
			running = false
			return
		elseif code == keys.n and held[ keys.leftCtrl ] then
			change_buffer( new_buffer( "", nil, buffer_view.width, buffer_view.height ) )
			return
		end

		-- * General keybindings * --

		if code == keys.p and held[ keys.leftCtrl ] then
			show_go_to_anything()
		end

		local buff = widget and widget.buffer or selected_buffer
		if not buff then return end

		-- * Editing-related keybindings * --

		local carets = buff.carets

		-- FIXME: lines are counted from 1
		-- wait - does that need a fix?

		-- TODO: variable width characters
		-- FIXME: change to motions instead of hardcoded event handling
		-- okay now it is motions but still manual for loops - ewww
		local caret_moved = true
		if code == keys.left then
			if held[ keys.leftCtrl ] then
				for _, caret in pairs( carets ) do
					caret.motion.jump.by.words.left()
				end
			else
				for _, caret in pairs( carets ) do
					caret.motion.simple.left()
				end
			end
		elseif code == keys.right then
			if held[ keys.leftCtrl ] then
				for _, caret in pairs( carets ) do
					caret.motion.jump.by.words.right()
				end
			else
				for _, caret in pairs( carets ) do
					caret.motion.simple.right()
				end
			end
		-- FIXME: need better logic for widget buffers
		elseif not widget and code == keys.up then
			for _, caret in pairs( carets ) do
				caret.motion.simple.up()
			end
		elseif not widget and code == keys.down then
			for _, caret in pairs( carets ) do
				caret.motion.simple.down()
			end
		elseif not widget and code == keys.space and held[ keys.leftCtrl ] then
			if #carets > 0 then
				show_autocompletion_popup( buff, carets[ 1 ] )
			end
		elseif code == keys.home then
			local line_or_file = held[ keys.leftCtrl ] and "file" or "line"

			for _, caret in pairs( carets ) do
				caret.motion.jump.to.beginning_of[ line_or_file ]()
			end
		elseif code == keys[ "end" ] then
			local line_or_file = held[ keys.leftCtrl ] and "file" or "line"

			for _, caret in pairs( carets ) do
				caret.motion.jump.to.end_of[ line_or_file ]()
			end
		elseif code == keys.insert then
			buff.mode = buff.mode == "insert" and "replace" or "insert"
		elseif code == keys.enter and not buff.no_newline then
			for _, caret in pairs( carets ) do
				edit[ buff.mode ]( caret, "\n" )
			end
		elseif code == keys.backspace then
			for _, caret in pairs( carets ) do
				edit.remove( caret, 1, true )
			end
		elseif code == keys.delete then
			for _, caret in pairs( carets ) do
				edit.remove( caret, 1, false )
			end
		elseif code == keys.b and held[ keys.leftCtrl ] then
			for _, caret in pairs( carets ) do
				show_bytecode( buff, caret )
			end
		elseif code == keys.pageUp then
			for _, caret in pairs( carets ) do
				caret.motion.jump.to( caret[ 2 ] - buffer_view.height + 1 ).line()
			end
		elseif code == keys.pageDown then
			for _, caret in pairs( carets ) do
				caret.motion.jump.to( caret[ 2 ] + buffer_view.height - 1 ).line()
			end
		elseif code == keys.f1 then
			dirt.full_repaint_required = true
			buff.carets = { carets[ 1 ] }
			buff:scroll_to_caret( carets[ 1 ] )
		elseif code == keys.w and held[ keys.leftCtrl ] then
			close_buffer( buff )
		elseif not widget and code == keys.s and held[ keys.leftCtrl ] then
			dirt.tabs_repaint_required = true
			if not buff:save() then
				show_status "Saving failed"
			end
		elseif code == keys.tab then
			if held[ keys.leftCtrl ] then
				local step = held[ keys.leftShift ] and -1 or 1

				for i = step > 0 and 1 or #open_buffers, step > 0 and #open_buffers or 1, step do
					if open_buffers[ i ] == buff then
						if i == #open_buffers and step > 0 then
							i = 0
						elseif i == 1 and step < 0 then
							i = #open_buffers + 1
						end

						change_buffer( open_buffers[ i + step ] or buff )
						break
					end
				end
			else
				local tab_str = settings.translate_tabs_to_spaces
					and ( " " ):rep( settings.tab_width )
					or "\t"

				for _, caret in pairs( carets ) do
					edit[ buff.mode ]( caret, tab_str )
				end
			end
		else
			caret_moved = false
		end

		if widget then
			widget.listener:handle_and_return( "key", code )
		end

		if caret_moved then
			dirt.status_bar_repaint_required = true
			buff:fix_carets()
		end
	end )
	:register_callback( "key_up", function( _, code )
		held[ code ] = false
	end )

status_bar_listener
	:register_callback( "mouse_click", function( _, button, x, _ )
		if button == 1 then
			if selected_buffer and x < w / 2 then
				if #selected_buffer.carets > 0 then
					selected_buffer.carets[ 1 ].motion.jump.to.beginning_of.next_issue()
				end
			end
		end
	end )

main_listener
	:register_callback( "resume_luacheck", function( _, i )
		local buff = open_buffers[ i ]
		if not buff then return end

		local lint_routine = buff.lint_routine
		if not lint_routine then return end

		if type( lint_routine ) ~= "thread" then
			log( "Expected thread for lint_routine, got ", type( lint_routine ) )
			return
		end

		local ok, state, result = coroutine.resume( lint_routine )
		-- TODO: better logging
		-- log( "resume:", ok, state, result )

		if not ok then
			log "luacheck crashed" ( state )
			buff.lint_routine = nil
		else
			if state == "result" then
				process_lint_results( buff, result )
				dirt.full_repaint_required = true
				buff.lint_state = nil
			elseif state == "luacheck.lexer.advance" then
				buff.lint_state = "parsing " .. math.floor( 100 * result.offset / #result.src ) .. "%"
				dirt.status_bar_repaint_required = true
			elseif state == "luacheck.analysis.advance" then
				buff.lint_state = "analysis " .. math.floor( 100 * result ) .. "%"
				dirt.status_bar_repaint_required = true
			else
				-- buff.lint_state = state
			end
		end
	end )

change_theme "advanced"

change_buffer(
	new_buffer( [[
local full_repaint_required, theme
local themes = false

--- Change the theme in use.
-- @param new_theme	The name of the new theme
-- @return nil
local function change_theme( new_theme )
	full_repaint_required = true
	theme = themes[ new_theme ] or themes.advanced
end

foo. = 3

change_theme( full_repaint_required
              and "something"
              or theme )
]], nil, buffer_view.width, buffer_view.height ) )

os.queueEvent "load"
while running do
	if selected_buffer and type( selected_buffer.lint_routine ) == "thread" then
		if coroutine.status( selected_buffer.lint_routine ) == "suspended" then
			local index
			-- FIXME: seriously? O(n)?
			for i = 1, #open_buffers do
				if open_buffers[ i ] == selected_buffer then
					index = i
					break
				end
			end

			os.queueEvent( "resume_luacheck", index )
		end
	end

	main_listener:handle_and_return( os.pullEvent() )

	-- FIXME: this is ugly, move this all into a function
	if widget then
		open_buffers[ #open_buffers + 1 ] = selected_buffer
	end

	local has_dirty_lines = false
	for i = 1, #open_buffers do
		if type( open_buffers[ i ].highlight_routine ) == "thread" then
			local ok, err = coroutine.resume( open_buffers[ i ].highlight_routine )
			if not ok then
				log( ""
					.. "highlight_routine for buffer #" .. i
					.. " (" .. tostring( open_buffers[ i ].title )
					.. "/" .. tostring( open_buffers[ i ].displayed_title ) .. ")"
					.. " ended with '" .. tostring( err ) .. "', removing"
				)

				open_buffers[ i ].highlight_routine = nil
				open_buffers[ i ].scheduled_highlight_data = nil
				dirt.full_repaint_required = true
			else
				has_dirty_lines = true
			end
		end
	end

	if widget then
		table.remove( open_buffers )
	end

	if has_dirty_lines then
		-- FIXME: we need an end of queue marker to process user events before highlight_steps
		os.queueEvent "highlight_step"
	end

	redraw()
end

utils.close_log()
