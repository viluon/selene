
local utils = require "utils"

local settings = {
	tab_width = 4;
	max_tab_title_length = 24;
	relint_delay = 1;	-- 1 second
	-- makes sense with the second one but not alone
	highlight_line_with_caret = false;
	highlight_line_number_with_caret = true;
	status_display_duration = 3;
	filesystem_reindex_interval = 15;
	vertical_scroll_speed = 1;
	horizontal_scroll_speed = 1;
	word_separators = "./\\()\"'-:,.;<>~!@#$%^&*|+=[]{}`~?";
	show_whitespace = false;
	force_block_caret = false;
	translate_tabs_to_spaces = false;
}

utils.read "/yelua/settings.tbl"
	:get( function( str )
		utils.log "Read settings file"
		utils.parse_table( str )
			:get( function( tbl )
				utils.log "Loaded settings"

				local custom_count = 0
				local applied = 0
				for k, v in pairs( tbl ) do
					-- TODO: type match may be too restrictive
					if type( v ) == type( settings[ k ] ) then
						applied = applied + 1
						settings[ k ] = v
					else
						utils.log(
							"warn: key " .. k .. " tried to override " .. type( settings[ k ] ) .. " with " .. type( v )
						)
					end

					custom_count = custom_count + 1
				end

				utils.log( "Applied " .. applied .. " out of " ..  custom_count .. " custom setting keys" )
			end )
			:catch( function( ... )
				utils.log "Failed to load settings" ( ... )
			end )
			:evaluate()
	end )
	:catch( function( ... )
		utils.log "Settings file not found, using defaults" ( ... )
	end )
	:evaluate()

local escaped_separators = ""
for i = 1, #settings.word_separators do
	escaped_separators = escaped_separators
	     .. "%" .. settings.word_separators:sub( i, i )
end
settings.word_separators = escaped_separators

return settings
