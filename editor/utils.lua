
local promise = require "promise"
local log_file = io.open( "/yelua/~log.txt", "a" )

--- Write to the log file.
-- @param ...	Values to write
-- @return Itself, for `log "line 1" "line 2"`
local function log( ... )
	local log_args = { ... }

	xpcall( function()
		-- TODO: use stack_trace.lua
		error( "", 4 )
	end, function( err )
		local args = ""

		for i = 1, #log_args do
			args = args .. tostring( log_args[ i ] ) .. ( i == #log_args and "" or " " )
		end

		log_file:write(
			( "[%6d] " ):format( os.clock() )
			.. err
			.. args
			.. "\n"
		)

		log_file:flush()
	end )

	return log
end

--- Parse a Lua table definition.
-- @param str	The serialized table
-- @return A promise
local function parse_table( str )
	return promise.new( function( resolve, reject )
		local fn, err = loadstring( "return " .. str, "table" )

		if not fn then
			return reject( err )
		end

		setfenv( fn, {} )
		local ok, result = pcall( fn )

		if not ok then
			return reject( result )
		end

		return resolve( result )
	end )
end

--- Read a file.
-- @param path	The path to the file
-- @return A special kind of promise, with the to_string() and to_lines() methods available.
local function read( path )
	return promise.new( function( resolve, reject )
		local f, err = io.open( path, "r" )
		if not f then
			return reject( err )
		end

		local ok, contents = pcall( f.read, f, "*a" )
		if not ok then
			return reject( contents )
		end

		return resolve( contents )
	end )
end

--- Close the log file handle.
-- @return nil
local function close_log()
	log "--- Closing Selene log file ---"
	log_file:close()
end

--- Create a shallow copy of a Lua value (non-recursive).
-- @param value	The value to copy
-- @return The copy of the original value
local function shallow_copy( value )
	local t = type( value )

	if t == "table" then
		local copy = {}
		for k, v in pairs( value ) do
			copy[ k ] = v
		end

		return copy
	else
		return value
	end
end

--- Prints tables and other fun stuff.
-- @return nil
local function deep_print( value, indent, no_newline )
	indent = indent or 1
	local t = type( value )
	local INDENT_SIZE = 4

	local result = ""

	if t == "table" then
		result = result .. ( "{" ) .. "\n"

		for k, v in pairs( value ) do
			result = result .. ( ( " " ):rep( indent * INDENT_SIZE ) )

			if type( k ) == "string" and k:match( "^[%a_][%w_]*$" ) then
				result = result .. ( k )
			else
				result = result .. ( "[ " )
				result = result .. deep_print( k, indent + 1, true )
				result = result .. ( " ]" )
			end

			result = result .. ( " = " )
			result = result .. deep_print( v, indent + 1, true )
			result = result .. ( ";" ) .. "\n"
		end

		result = result .. ( ( " " ):rep( ( indent - 1 ) * INDENT_SIZE ) .. "}" )

	elseif t == "string" then
		result = result .. ( ( "%q" ):format( value ) )
	else
		result = result .. ( tostring( value ) )
	end

	if not no_newline then
		result = result .. "\n"
	end

	return result
end

log "--- Beginning Selene log file ---"

return {
	shallow_copy = shallow_copy;
	parse_table = parse_table;
	deep_print = deep_print;
	close_log = close_log;
	read = read;
	log = log;
}
