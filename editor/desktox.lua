
local desktox = dofile "/desktox/init.lua" "snake_case"

local buffer = desktox:load "/desktox/buffer/init.lua"

return {
	desktox    = desktox;
	handler    = desktox:load "/desktox/handler/init.lua";
	buffer_dtt = dofile "/desktox/buffer/dtt.lua";
	buffer     = buffer;
}
