
-- Selene's rope library

local rope = {}
local rope_meta = { __index = rope }

local node = {}
local node_meta = { __index = node }

--- Create a new rope.
-- @return The new instance
function rope.new()
	local n = setmetatable( {
		root = node.new()
	}, rope_meta )

	return n
end

--- Create a new rope node.
-- @param str	(Optional) The node contents
-- @return The new instance
function node.new( str )
	local n = setmetatable( {
		str = str
	}, node_meta )

	return n
end
