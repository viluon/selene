
-- luacheck:globals fs.find

local settings = require "settings"
local log = require "utils".log

local env = {
	computer_craft = {
		std = "lua51c";
		max_cyclomatic_complexity = settings.max_cyclomatic_complexity or 12;
		read_globals = {};
	};
}

--- Load configuration files for different environments (CC, OC, plain Lua, ...).
-- @return nil
local function load_environment_config()
	-- FIXME: another hardcoded path
	for _, path in pairs( fs.find "/yelua/editor/envs/*" ) do
		local file = io.open( path, "r" )
		local functions = file:read "*a" .. "\n"
		file:close()

		local env_name = path:match "^.*/([^/]+)%.txt$"
		log( "loading environment", env_name )
		env[ env_name ] = env[ env_name ] or {}

		for api, key in functions:gmatch "([%w_]+)%.?([%w_]*)\n" do
			if key and #key > 0 then
				-- an API function or constant
				env[ env_name ].read_globals[ api ] =
					env[ env_name ].read_globals[ api ] or { fields = {} }
				table.insert( env[ env_name ].read_globals[ api ].fields, key )
			else
				-- a global function or constant
				table.insert( env[ env_name ].read_globals, api )
			end
		end
	end
end

load_environment_config()

return env
