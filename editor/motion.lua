
local dirt = require "dirt"
local log = require "utils".log
local settings = require "settings"

local max, min = math.max, math.min
local empty_function = function() end
local motion_metatable
local motion

local caret_metatable = {
	__index = function( caret, k )
		if k == "motion" then
			motion._buffer = caret.buffer
			motion._caret = caret
			return motion
		end

		if k ~= "selection" and k ~= "desired_x" then
			log( "warn: weird __index into a caret: ", k )
		end

		return nil
	end,
	__lt = function( this, that )
		if this[ 2 ] == that[ 2 ] then
			return this[ 1 ] < that[ 1 ]
		end

		return this[ 2 ] < that[ 2 ]
	end,
	__eq = function( this, that )
		return this[ 2 ] == that[ 2 ] and this[ 1 ] == that[ 1 ]
	end,
}

local motion_accessor_metatable = {
	__index = function( t, k )
		local fn = motion[ rawget( t, "beginning" ) ~= nil and ( k .. "_be" ) or k ]

		if fn then
			dirt.full_repaint_required = true

			-- rawgets, since we don't want to proxy through motion_accessor __index again
			local jump_by_callback = rawget( t, "jump_by_callback" )
			local beginning = rawget( t, "beginning" )

			fn(
				motion._caret,
				beginning ~= nil
					and beginning
					or jump_by_callback
						and jump_by_callback( k )
						or rawget( t, "n" ),
				rawget( t, "selecting" )
			)

			-- FIXME: eeeh, is this a hack?
			motion._caret.buffer:scroll_to_caret( motion._caret )

			return empty_function
		end

		log( "warn: could not find motion function ", k )

		return nil
	end
}

local words_accessor = setmetatable( { jump_by_callback = function( key )
	local line_str = motion._buffer.lines[ motion._caret[ 2 ] ].str
	if key == "right" then
		line_str = line_str:sub( motion._caret[ 1 ] + 1, -1 )
	elseif key == "left" then
		line_str = line_str:sub( 1, motion._caret[ 1 ] ):reverse()
	else
		log( "warn: jump.by.words isn't followed by 'left' or 'right':", key )
	end

	local _, e = line_str:find( "^%s*[" .. settings.word_separators .. "]*[^" .. settings.word_separators .. "%s]*" )
	if e == 0 then
		return 1
	end

	return e or 1
end }, motion_accessor_metatable )

local jump_by_metatable = {
	__index = function( _, k )
		if k == "words" then
			return words_accessor
		end

		log( "warn: jump.by isn't followed by 'words':", k )

		return nil
	end;
}

local jump_by = setmetatable( {}, jump_by_metatable )

local beginning_of_accessor = setmetatable( { beginning = true }, motion_accessor_metatable )
local end_of_accessor = setmetatable( { beginning = false }, motion_accessor_metatable )

local jump_to_metatable = {
	__index = function( _, k )
		if k == "beginning_of" then
			return beginning_of_accessor
		elseif k == "end_of" then
			return end_of_accessor
		end

		log( "warn: jump.to is followed by neither 'beginning_of' nor 'end_of': ", k )

		return nil
	end,
	__call = function( _, n )
		return setmetatable( { n = n }, motion_accessor_metatable )
	end
}

local jump_to = setmetatable( {}, jump_to_metatable )
--[[ TODO:
	* jump.to.letter "f".right()
	* jump.to.beginning_of.line()
	* jump.to.end_of.file()
--]]
local jump_by_proxy = {
	by = jump_by;
	to = jump_to;
}

motion_metatable = {
	__index = function( _, k )
		if k == "simple" then
			return setmetatable( { n = 1 }, motion_accessor_metatable )
		elseif k == "jump" then
			return jump_by_proxy
		end

		log( "warn: motion is neither 'simple' nor 'jump': ", k )

		xpcall( function()
			error( "", 5 )
		end, function( err )
			log( err )
		end )

		return nil
	end
}

motion = setmetatable( {}, motion_metatable )

--- Clamp the x coordinate of a caret.
-- @param caret	description
-- @param low_limit	description
-- @param high_limit	description
-- @param value	description
-- @param no_desires	Don't touch the desired values
-- @return Minimum of {low_limit, value}
local function clamp_caret_x( caret, low_limit, high_limit, value, no_desires )
	if max( low_limit, value ) == low_limit then
		if not no_desires and not caret.desired_x then
			caret.desired_x = value
		end

		return low_limit
	elseif min( high_limit, value ) == high_limit then
		if not no_desires and not caret.desired_x then
			caret.desired_x = value
		end

		return high_limit
	end

	return value
end

--- Move a caret left.
-- @param caret	description
-- @param n	description
-- @param selecting	description
-- @return motion
function motion.left( caret, n, selecting )
	local buff = motion._buffer

	if selecting then
		-- TODO
	end

	while n > 0 do
		if caret[ 1 ] <= 0 then
			if caret[ 2 ] > 1 then
				caret[ 2 ] = caret[ 2 ] - 1
				caret[ 1 ] = #buff.lines[ caret[ 2 ] ].str
				dirt.full_repaint_required = true
			end
		else
			caret[ 1 ] = caret[ 1 ] - 1
		end

		n = n - 1
	end

	-- caret.desired_x = nil
	caret.desired_x = buff:index_to_screen_space( caret[ 2 ], caret[ 1 ] )

	return motion
end

--- Move a caret right.
-- @param caret	description
-- @param n	description
-- @param selecting	description
-- @return motion
function motion.right( caret, n, selecting )
	local buff = motion._buffer

	if selecting then
		-- TODO
	end

	while n > 0 do
		if caret[ 1 ] >= #buff.lines[ caret[ 2 ] ].str then
			if caret[ 2 ] < buff.n_lines then
				caret[ 1 ] = 0
				caret[ 2 ] = caret[ 2 ] + 1
				dirt.full_repaint_required = true
			end
		else
			caret[ 1 ] = caret[ 1 ] + 1
		end

		n = n - 1
	end

	-- caret.desired_x = nil
	caret.desired_x = buff:index_to_screen_space( caret[ 2 ], caret[ 1 ] )

	return motion
end

--- Move a caret up.
-- @param caret	description
-- @param n	description
-- @param selecting	description
-- @return motion
function motion.up( caret, n, selecting )
	local buff = motion._buffer

	if selecting then
		-- TODO
	end

	if caret[ 2 ] > 1 then
		caret[ 2 ] = caret[ 2 ] - 1

		local mapped = buff:index_to_screen_space(
			caret[ 2 ],
			caret.desired_x
				and buff:screen_space_to_index( caret[ 2 ], caret.desired_x )
				or caret[ 1 ]
		)

		caret[ 1 ] = clamp_caret_x(
			caret,
			0,
			#buff.lines[ caret[ 2 ] ].str,
			buff:screen_space_to_index( caret[ 2 ], mapped )
		)

		dirt.full_repaint_required = true
	else
		caret[ 1 ], caret.desired_x = 0, 0
	end

	return motion
end

--- Move a caret down.
-- @param caret	description
-- @param n	description
-- @param selecting	description
-- @return motion
function motion.down( caret, n, selecting )
	local buff = motion._buffer

	if selecting then
		-- TODO
	end

	if caret[ 2 ] < buff.n_lines then
		caret[ 2 ] = caret[ 2 ] + 1

		local mapped = buff:index_to_screen_space(
			caret[ 2 ],
			caret.desired_x
				and buff:screen_space_to_index( caret[ 2 ], caret.desired_x )
				or caret[ 1 ]
		)

		caret[ 1 ] = clamp_caret_x(
			caret,
			0,
			#buff.lines[ caret[ 2 ] ].str,
			buff:screen_space_to_index( caret[ 2 ], mapped )
		)

		dirt.full_repaint_required = true
	else
		caret[ 1 ] = #buff.lines[ caret[ 2 ] ].str
		caret.desired_x = caret[ 1 ]
	end

	return motion
end

--- Move to a certain line.
-- @param caret	description
-- @param number	description
-- @return motion
function motion.line( caret, number )
	local buff = motion._buffer

	caret[ 2 ] = max( 1, min( buff.n_lines, number ) )
	motion.line_be( caret, true, false )

	return motion
end

--- Move to the beginning or end of a line.
-- @param caret	description
-- @param beginning	description
-- @param selecting	description
-- @return motion
function motion.line_be( caret, beginning, selecting )
	local buff = motion._buffer

	if selecting then
		-- TODO
	end

	if beginning then
		local first_word_boundary = buff.lines[ caret[ 2 ] ].str:match "^%s*()%S"
		first_word_boundary = first_word_boundary and first_word_boundary - 1

		-- this condition works even if first_word_boundary == nil
		caret[ 1 ] = caret[ 1 ] == first_word_boundary and 0 or first_word_boundary or 0
		caret.desired_x = nil
	else
		caret[ 1 ] = #buff.lines[ caret[ 2 ] ].str
		caret.desired_x = math.huge
	end

	return motion
end

--- Move to the beginning or end of the file.
-- @param caret	description
-- @param beginning	description
-- @param selecting	description
-- @return motion
function motion.file_be( caret, beginning, selecting )
	local buff = motion._buffer

	if selecting then
		-- TODO
	end

	if beginning then
		caret[ 2 ] = 1
		caret[ 1 ] = 0
		caret.desired_x = nil
	else
		caret[ 2 ] = buff.n_lines
		motion.line_be( caret, false, false )
	end

	return motion
end

--- Move a caret to the next issue in the buffer, if there is any.
-- @param caret	description
-- @param beginning	description
-- @param selecting	description
-- @return motion
function motion.next_issue_be( caret, beginning, selecting )
	local buff = motion._buffer

	if selecting then
		-- TODO
	end

	for y = caret[ 2 ], buff.n_lines do
		local local_issues = buff.lines_with_issues[ y ]
		if local_issues then
			for i = 1, #local_issues do
				if y ~= caret[ 2 ] or local_issues[ i ].column > caret[ 1 ] + 1 then
					caret.motion.jump.to( y ).line()

					if beginning then
						caret[ 1 ] = max( local_issues[ i ].column - 1, 0 )
					else
						caret[ 1 ] = max( local_issues[ i ].end_column, 0 )
					end

					buff:scroll_to_caret( caret )

					return motion
				end
			end
		end
	end

	return motion
end

return {
	caret_metatable = caret_metatable;
	motion = motion;
}
