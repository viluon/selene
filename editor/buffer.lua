
-- Text buffer library for <insert name of the editor>

local settings = require "settings"
local caret_metatable = require "motion".caret_metatable
local utils = require "utils"
local dirt = require "dirt"

local max, min = math.max, math.min

local lib_meta = {}
local buffer = setmetatable( {}, lib_meta )

--- Create a new buffer.
-- @param str	description
-- @param title	description
-- @return The new buffer
function buffer.new( str, title, width, height )
	local buff = setmetatable( {
		title = title;
		mode = "insert";
		lines = {};
		scroll = { 0, 0 };
		n_lines = 0;
		width = width;
		height = height;
		tab_width = settings.tab_width;
	}, { __index = buffer } )

	buff:reset_carets()

	str = str or ""
	str = str .. "\n"
	-- FIXME: issues with CC's file loading (no newline at end of file)
	-- if #str == 0 or str:sub( -1, -1 ) ~= "\n" then
	-- end

	for line in str:gmatch "(.-)\n" do
		buff.n_lines = buff.n_lines + 1
		buff.lines[ buff.n_lines ] = {
			str = line
		}
	end

	return buff
end

--- Remove all carets from this buffer.
-- @return nil
function buffer:reset_carets()
	self.carets = {
		--[[ A note on carets:
			* the structure is { x, y, desired_x = n, desired_y = n, buffer = buff }
			* desired_x is kept for movement across lines of different lengths
			* desired_y is kept for file reloads and possibly other
			  external full buffer updates (where if no motion is performed,
			  you'd like the caret's position fully restored)
			* desired_[xy] are in SCREEN SPACE!!!
		--]]
	}
end

--- Offset scroll so that a caret is visible.
-- @param caret	description
-- @return nil
function buffer:scroll_to_caret( caret )
	local mapped_x = self:index_to_screen_space( caret[ 2 ], caret[ 1 ] )

	self.scroll[ 1 ] = -max(
		mapped_x - self.width + 1,
		min( mapped_x, -self.scroll[ 1 ] )
	)

	self.scroll[ 2 ] =  max(
		caret[ 2 ] - self.height,
		min( caret[ 2 ] - 1, self.scroll[ 2 ] )
	)
end

--- Map the real position in the string to the displayed line coordinate.
-- TODO could be merged with screen_space_to_index()
-- @param n	The line number
-- @param x	The index
-- @return The index mapped to the displayed line
function buffer:index_to_screen_space( n, x )
	if not self.lines[ n ].formatted then
		return x
	end

	local mapped = 0
	for _, fragment in pairs( self.lines[ n ].formatted ) do
		local new_x
		if fragment.length then
			new_x = x - 1
		else
			new_x = x - #fragment[ 2 ]
		end

		if new_x < 0 then
			return mapped + x
		else
			x = new_x
			mapped = mapped + #fragment[ 2 ]
		end
	end

	return mapped + x
end

--- Map the displayed line coordinate to the real position in the string.
-- @param x	description
-- @return The index of x in the original string
function buffer:screen_space_to_index( n, x )
	if not self.lines[ n ].formatted then
		return x
	end

	local mapped = 0
	for _, fragment in pairs( self.lines[ n ].formatted ) do
		local new_x
		if fragment.length then
			new_x = x - fragment.length
		else
			new_x = x - #fragment[ 2 ]
		end

		if new_x < 0 then
			if fragment.length then
				-- the click hit a variable width character
				-- find out which side is closer and go there
				if x < fragment.length / 2 then
					return mapped
				else
					return mapped + 1
				end
			else
				return mapped + x
			end
		else
			x = new_x
			mapped = mapped + ( fragment.length and 1 or #fragment[ 2 ] )
		end
	end

	return mapped + x
end

--- Create a new caret and add it to the carets array.
-- @param buff	The buffer to add the caret for
-- @param x	The x coordinate of the caret
-- @param y	The y coordinate of the caret
-- @return The newly created caret
function buffer:add_caret( x, y )
	local caret = setmetatable( {
		x, y,
		desired_x = self:index_to_screen_space( y, x );
		buffer = self;
	}, caret_metatable )

	table.insert( self.carets, caret )
	-- TODO: optimise this, only needs to insert nicely (O(n) vs O(n log n))
	self:fix_carets()

	dirt.full_repaint_required = true

	return caret
end

--- Sort carets and join duplicates.
-- @return nil
function buffer:fix_carets()
	table.sort( self.carets )

	-- TODO: preserve carets with (larger) selection
	-- remove duplicate carets
	for i = #self.carets, 2, -1 do
		if self.carets[ i ] == self.carets[ i - 1 ] then
			table.remove( self.carets, i )
		end
	end
end

--- Get a string representation of this buffer's contents.
-- @return This buffer's contents ('\n'-separated lines)
function buffer:tostring()
	local str = ""
	for i = 1, self.n_lines do
		str = str .. self.lines[ i ].str .. "\n"
	end

	return str
end

--- Save the buffer contents to a file.
-- @return A boolean indicating whether the operation succeeded
function buffer:save()
	if not self.path then
		utils.log "No path specified, saving to /tmp.txt"
		self.path = "/tmp.txt"
	end

	local f, err = io.open( self.path, "w" )

	if not f then
		utils.log( "Failed to write to " .. tostring( self.path ) .. ":" )( err )
		return false
	end

	f:write( self:tostring() )
	f:close()

	self.saved = true

	return true
end

return buffer
