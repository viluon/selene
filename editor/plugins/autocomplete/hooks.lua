
-- luacheck:globals hook scope

local autocomplete = require "plugins.autocomplete"

hook: setup {
	-- Autocompletion is buffer-specific
	scope = scope.buffer;
	keybindings = {
		show = "ctrl+space";
		accept = "enter";
	};
	triggers = {
		"keybindings.show";
	};
}

hook: on_key "show"   ( autocomplete.show )
hook: on_key "accept" ( autocomplete.accept )
hook: on_click "left" ( autocomplete.click )

hook: on_render {
	-- TODO: before/after + top sort instead
	z_index = 3;
	draw = autocomplete.draw;
}
