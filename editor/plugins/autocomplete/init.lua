
local autocomplete = {}

--- Show the widget.
function autocomplete:show()
	self.should_draw = true
	self.enabled = true
end

--- Accept the selected suggestion.
function autocomplete:accept( state )
	state:insert( self.suggestions[ self.selected ] )
	self:disable()
end

--- Handle a click on the widget.
function autocomplete:click( state, event )
	local index = event.y - self.y + self.scroll

	if index > self.width
	or index < 1 then
		index = nil
	end

	if  event.x >= self.x
	and event.x <= self.x + self.width
	and index then
		self:accept( state )
	else
		self:disable()
	end
end

--- Hide (and disable) the widget.
function autocomplete:disable()
	self.should_draw = false;
	self.enabled = false;
end

return autocomplete
