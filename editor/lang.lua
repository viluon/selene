
local lang = {
	lua = {
		keywords = {
			[ "and" ] = true;
			[ "break" ] = true;
			[ "do" ] = true;
			[ "else" ] = true;
			[ "elseif" ] = true;
			[ "end" ] = true;
			[ "false" ] = true;
			[ "for" ] = true;
			[ "function" ] = true;
			[ "if" ] = true;
			[ "in" ] = true;
			[ "local" ] = true;
			[ "nil" ] = true;
			[ "not" ] = true;
			[ "or" ] = true;
			[ "repeat" ] = true;
			[ "return" ] = true;
			[ "then" ] = true;
			[ "true" ] = true;
			[ "until" ] = true;
			[ "while" ] = true;
		};
		opcodes = {
			"MOVE",
			"LOADK",
			"LOADBOOL",
			"LOADNIL",
			"GETUPVAL",
			"GETGLOBAL",
			"GETTABLE",
			"SETGLOBAL",
			"SETUPVAL",
			"SETTABLE",
			"NEWTABLE",
			"SELF",
			"ADD",
			"SUB",
			"MUL",
			"DIV",
			"MOD",
			"POW",
			"UNM",
			"NOT",
			"LEN",
			"CONCAT",
			"JMP",
			"EQ",
			"LT",
			"LE",
			"TEST",
			"TESTSET",
			"CALL",
			"TAILCALL",
			"RETURN",
			"FORLOOP",
			"FORPREP",
			"TFORLOOP",
			"SETLIST",
			"CLOSE",
			"CLOSURE",
			"VARARG"
		};

		control_characters = {};
	}
}

for char in ( "-<{[(/#~=;:*,.)]}>+" ):gmatch "." do
	lang.lua.control_characters[ char ] = true
end

return lang
