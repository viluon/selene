
-- luacheck:globals term.getSize term.current os.pullEvent os.queueEvent ccemux.milliTime sleep os.startTimer

local utils = require "utils"
local desktox = require "desktox"
local buffer = desktox.buffer
local colours = buffer.colours
local math = math

local debug = ""

local main_win = term.current()
local w, h = term.getSize()
local main_buffer = buffer.new( 0, 0, w, h, nil, colours.black )
local welcome_buffer = buffer.new( math.floor( w / 4 ), 0, math.ceil( w / 2 ), math.floor( h * ( 2 / 4 ) ), main_buffer )
local welcome_buffer_shown
local status_buffer = buffer.new(
	0, math.floor( h / 2 ) + 1,
	w, 1,
	main_buffer,
	colours.transparent_bg,
	colours.transparent_fg,
	"\0"
)
local particle_buffer = buffer.new( 0, 0, w * 2, h * 3, nil, colours.black )
local os_clock = os.clock

local title = "Selene"
local status = { "is", "downloading" }
local progress = 0.0
local bar_left_limit = 0.0
local rotation_speed = 1.3 * 2.3
local a = 0
local title_y_offset = 0
local welcome_buffer_y_offset = 0
-- special value to avoid extra :map() operation
local colour_radius = -1

-- animation init time (in seconds)
local INIT_TIME = 3.5

-- curvature
local c = -0.3

local report_progress
local steps = {
	{
		"is being downloaded",
		function()
			sleep( 0 )
			report_progress( 0.2 )
			sleep( 0 )
			report_progress( 0.4 )
			sleep( 0 )
			report_progress( 1.0 )
		end
	};
	{
		"is unpacking",
		function()
			sleep( 0 )
			report_progress( 0.8 )
			sleep( 1 )
			report_progress( 1.0 )
		end
	};
}

-- UUID for the report_progress yield
local PROGRESS_REPORT = {}

--- Report the progress of an installation step to the install coroutine.
-- @param n	description
function report_progress( n )
	coroutine.yield( PROGRESS_REPORT, n )
end

--- Draw the spirals.
-- @return nil
local function draw( zoom, near_scale, far_scale, r_bound, spin )
	local  sin,      cos,      exp,      pi,      max,      floor
	= math.sin, math.cos, math.exp, math.pi, math.max, math.floor

	local _w, _h = w, h
	local step = ( -1 / c ) / 40
	-- 100 iterations
	local limit = -100 * step
	-- pi * -1.5 * far_scale

	for theta = limit, pi * -1 * near_scale, step do
		-- r=e^{10-a}e^{c\cdot\theta}
		local r = 2.5 * exp( zoom + c * theta )
		local spinning_theta = theta + spin

		if r < r_bound then
			local x1 = r * cos( spinning_theta ) + _w
			local y1 = r * sin( spinning_theta ) + 1.5 * _h

			local x2 = r * cos( spinning_theta - pi ) + _w
			local y2 = r * sin( spinning_theta - pi ) + 1.5 * _h

			particle_buffer
				:write( floor( x1 ), floor( y1 ), "r", colours.light_grey, colours.black )
				:write( floor( x2 ), floor( y2 ), "r", colours.light_grey, colours.black )
		end
	end
end

local grey_pixel = buffer.pixel( colours.grey )
local black_pixel = buffer.pixel( colours.black )
local spiral_pixel = buffer.pixel( colours.light_grey, colours.black, "r" )
--- Shade the buffer so that colour only appears in colour_radius around the centre.
-- @param self	description
-- @param x	description
-- @param y	description
-- @param pixel	description
-- @return nil
local function colour_circle_shader( self, x, y, pixel )
	x = x - w
	y = y - h * 1.5
	if pixel == black_pixel or x * x + y * y < colour_radius * colour_radius then
		if pixel == spiral_pixel then
			return black_pixel
		end
		return pixel
	else
		if pixel == spiral_pixel then
			return pixel
		end

		return grey_pixel
	end
end

--- Change the current installation status.
-- @param str	The status string
-- @return nil
local function change_status( str )
	local new_status = {}
	for word in str:gmatch "%w+" do
		new_status[ #new_status + 1 ] = word
	end

	local differing_index = 1

	for i = 1, #status do
		if #new_status < i or status[ i ] ~= new_status[ i ] then
			differing_index = i
			break
		end
	end

	for i = #status, differing_index, -1 do
		table.remove( status, i )
		sleep( 0.2 )
	end

	sleep( 0.2 )

	for i = differing_index, #new_status do
		status[ i ] = new_status[ i ]
		sleep( 0.2 )
	end
end

--- Draw the installation status.
-- @return nil
local function print_status()
	status_buffer:clear(
		colours.transparent_bg,
		colours.transparent_fg,
		"\0"
	)

	local cursor = 0
	local status_width = #table.concat( status, " " )
	local horizontal_offset = math.floor( w / 2 - status_width / 2 )

	for i = 1, #status do
		local word = status[ i ]

		local  bar_left_index = math.max( 0, math.floor( bar_left_limit * status_width ) - cursor )
		local bar_right_index = math.max( 0, math.floor( progress * status_width ) - cursor )

		local  first_part = word:sub( 1, bar_left_index )
		local second_part = word:sub( #first_part + 1, bar_right_index )
		local  third_part = word:sub( #first_part + #second_part + 1 )

		status_buffer:write(
			horizontal_offset + cursor,
			0,
			first_part,
			colours.black,
			colours.grey
		):write(
			horizontal_offset + #first_part + cursor,
			0,
			second_part,
			colours.black,
			colours.green
		):write(
			horizontal_offset + #first_part + #second_part + cursor,
			0,
			third_part,
			colours.black,
			colours.grey
		)

		cursor = cursor + #word + 1
	end
end

--- Get the current time in the most precise format possible.
-- @return The current time in seconds (since platform start up)
local function clock()
	return os_clock()
end

if ccemux and ccemux.milliTime then
	local milli_time = ccemux.milliTime

	function clock()
		return milli_time() / 1000.0
	end
end

--- Perform the installation steps.
-- @return nil
local function install_steps()
	for i, step in ipairs( steps ) do
		progress = 0.0
		bar_left_limit = 0.0
		change_status( step[ 1 ] )
		local coro = coroutine.create( step[ 2 ] )

		local data = {}
		while true do
			local ev = { coroutine.yield( unpack( data ) ) }
			data = { coroutine.resume( coro, unpack( ev ) ) }
			local ok = table.remove( data, 1 )

			if not ok then
				break
			end

			if data[ 1 ] == PROGRESS_REPORT then
				progress = data[ 2 ]
			end
		end

		sleep( 0.15 )
		local out_anim_start = clock()
		local out_anim_duration = 0.7

		while clock() - out_anim_start < out_anim_duration do
			coroutine.yield()
			bar_left_limit = ( clock() - out_anim_start ) / out_anim_duration

			if i == #steps then
				c = 0.7 - math.exp( ( ( clock() - out_anim_start ) / out_anim_duration ) * 2.2 )
			end
		end
	end
end

--- Pick a highlight colour with a uniform distribtion.
-- @param theme	description
-- @return The chosen colour
local function pick_theme_highlight( theme )
	local keys = {}
	for k in pairs( theme.highlight ) do
		keys[ #keys + 1 ] = k
	end

	return theme.highlight[ keys[ math.random( 1, #keys ) ] ]
end

--- Generate a "line of code" - a line of filled/empty pixels resembling code.
-- @return nil
local function generate_line()
	local themes = require "themes"
	local theme  = themes.advanced
	local rnd = math.random

	local function pick_colour()
		return rnd() > 0.5
			and pick_theme_highlight( theme )
			or theme.secondary
	end

	local line = buffer.new( 0, 0, w * 2 - rnd( 3, 8 ), 1, nil, theme.background )

	local x = rnd( 2, 5 )
	while x < w * 2 - 2 do
		local current_colour = pick_colour()
		local length = rnd() > 0.8 and rnd( 8, 15 ) or rnd( 3, 7 )
		line:clear_line( 0, current_colour, theme.background, "_", x, x + length )

		local whitespace = rnd( 2, 5 )
		x = x + length + whitespace
	end

	return line
end

--- Render an array of "code lines".
-- @param lines	description
-- @return nil
local function render_lines( lines, scroll, ratio )
	scroll = scroll or 0
	local y_offset_ratio = 4
	ratio = ratio or 0

	for y = 0, h * 1.5 do
		local x
		if y % 2 == 0 then
			x = -w * 2 * y_offset_ratio * ratio
		else
			x =  w * 2 * y_offset_ratio * ratio
		end

		x = x * ( 1 / y_offset_ratio + ( y / #lines ) / y_offset_ratio )

		if y * 3 + scroll ~= h * 3 - 1 and math.floor( ( ( y - 1 ) * 3 / 1.5 ) / 4 ) ~= math.floor( h / 4 ) then
			lines[ ( y + 1 ) % #lines + 1 ]:render( particle_buffer, math.floor( x ), y * 3 + scroll )
		end
	end
end

--- Launch the installed editor.
-- @return nil
local function launch()
	debug = math.random( 1, 10 )
end

--- Show a welcome message to the user.
-- @return nil
local function show_welcome_message( lines )
	welcome_buffer_shown = true
	local scroll_duration = 1.0
	local start = clock()

	local button_text = " Launch "
	local button_x = math.floor( welcome_buffer.width / 2 - #button_text / 2 )
	local char_hor  = "\140"
	local char_vert = "\149"
	welcome_buffer
		:clear( colours.black, colours.light_grey )
		:clear_line( 0, colours.black, colours.light_grey, char_hor )
		:clear_line( welcome_buffer.height - 1, colours.black, colours.light_grey, char_hor )
		:clear_column( 0, colours.light_grey, colours.black, char_vert )
		:clear_column( welcome_buffer.width - 1, colours.black, colours.light_grey, char_vert )
		:write( 0, 0, "\151", colours.light_grey )
		:write( 0, welcome_buffer.height - 1, "\138", colours.black, colours.light_grey )
		:write( welcome_buffer.width - 1, 0, "\148", colours.black, colours.light_grey )
		:write( welcome_buffer.width - 1, welcome_buffer.height - 1, "\133", colours.black, colours.light_grey )
		:write( 2, 1, "Congratulations!", colours.black, colours.white )
		:write( 2, 3, "Your Selene is ready", colours.black, colours.light_grey )
		:write( 2, 4, "to use.", colours.black, colours.light_grey )
		:clear_line(
			welcome_buffer.height - 4,
			colours.blue,
			colours.black,
			"\143",
			button_x,
			button_x + #button_text - 1
		):clear_line(
			welcome_buffer.height - 2,
			colours.black,
			colours.blue,
			"\131",
			button_x,
			button_x + #button_text - 1
		):write(
			button_x,
			welcome_buffer.height - 3,
			button_text,
			colours.blue,
			colours.white
		):write(
			button_x - 1,
			welcome_buffer.height - 3,
			"\149",
			colours.blue,
			colours.black
		):write(
			button_x + #button_text,
			welcome_buffer.height - 3,
			"\149",
			colours.black,
			colours.blue
		):write(
			button_x - 1,
			welcome_buffer.height - 2,
			"\130",
			colours.black,
			colours.blue
		):write(
			button_x - 1,
			welcome_buffer.height - 4,
			"\159",
			colours.blue,
			colours.black
		):write(
			button_x + #button_text,
			welcome_buffer.height - 2,
			"\129",
			colours.black,
			colours.blue
		):write(
			button_x + #button_text,
			welcome_buffer.height - 4,
			"\144",
			colours.black,
			colours.blue
		)

	while true do
		local ev = { coroutine.yield() }
		local now = clock()
		local dt = math.min( scroll_duration, now - start )
		local ratio = dt / scroll_duration

		if ratio < 0.5 then
			ratio = 0.5 * ( 2 * ratio ) ^ 2
		else
			ratio = -0.5 * ( 2 * ratio - 1 ) * ( 2 * ratio - 3 ) + 0.5
		end

		title_y_offset = -math.floor( ratio * ( h / 2 - 1 ) )
		render_lines( lines, 3 * title_y_offset - 1 )
		welcome_buffer_y_offset = -h * ( 2 / 3 ) * ratio

		if ev[ 1 ] == "mouse_click" then
			if ev[ 3 ] - 1 >= welcome_buffer.x1 + button_x
			and ev[ 4 ] - 1 >= h + title_y_offset + welcome_buffer.height - 4 then
				if ev[ 3 ] - 1 <= welcome_buffer.x1 + button_x + #button_text
				and ev[ 4 ] - 1 <= h + title_y_offset + welcome_buffer.height - 2 then
					launch()
				end
			end
		end
	end
end

--- Animate lines of code flying in from left&right.
-- @return nil
local function anim_lines_fly_in()
	local lines = {}

	for i = 1, h do
		lines[ i ] = generate_line()
	end

	local anim_duration = 2.4
	colour_radius = 0.0

	-- clock() - start < anim_duration
	local start = clock()
	local colour_start = start + 2
	local colour_duration = 0.5
	local wait_before_welcome = 1

	while true do
		coroutine.yield()
		local now = clock()
		local dt = math.min( now - start, anim_duration )
		local ratio = ( 1 - dt / anim_duration ) ^ 2.7

		render_lines( lines, 0, ratio )

		if now > colour_start then
			local colour_ratio = ( colour_start - now ) / colour_duration
			colour_radius = ( w + 1 ) * colour_ratio * colour_ratio

			if now > colour_start + colour_duration + wait_before_welcome then
				break
			end
		end
	end

	return show_welcome_message( lines )
end

--- The installation function (to be made into a coroutine).
-- @return nil
local function install()
	install_steps()
	change_status ""

	progress = 0.0
	local slowdown_duration = 3
	local target = a - a % math.pi + math.pi
	local org = a
	rotation_speed = 0
	local org_c = c
	local s = clock()

	while clock() - s < slowdown_duration do
		coroutine.yield()
		-- debug = a .. ", " .. c
		local dt = clock() - s
		local ratio = ( 1 - ( 1 - dt / slowdown_duration ) ^ 3 )
		a = org + ( target - org ) * ratio
		c = org_c + ( -2 ^ 9 - org_c ) * ratio
	end

	return anim_lines_fly_in()
end

local end_queued
local start_time = clock()
local last_time = start_time
local radius_limit = 500
local install_routine = coroutine.create( install )

-- The Glorious Game Loop
while true do
	if not end_queued then
		os.queueEvent "end_of_queue"
		end_queued = true
	end

	local ev = { os.pullEvent() }
	local now = clock()

	if ev[ 1 ] == "end_of_queue" then
		end_queued = false
	elseif ev[ 1 ] == "char" then
		break
	end

	if now ~= last_time then
		local dt = now - last_time

		-- local near_scale = math.min( 1.0, now - start_time )
		-- local far_scale = math.max( 1.0, now - start_time )

		particle_buffer:clear( colours.black )
		local zoom = math.max( 0, INIT_TIME - a / 2 ) * 0.6

		draw(
			1,
			zoom,
			2,
			radius_limit,
			-a + zoom * 3
		)

		if now - start_time > INIT_TIME and coroutine.status( install_routine ) == "suspended" then
			local ok, err = coroutine.resume( install_routine, unpack( ev ) )
			if not ok then
				error( err, 0 )
			end
			print_status()
		end

		if colour_radius > -1 then
			particle_buffer:map( colour_circle_shader )
		end

		particle_buffer:shrink( main_buffer )

		main_buffer:write(
			math.floor( w / 2 - #title / 2 ),
			math.floor( h / 2 ) + title_y_offset,
			title,
			colours.black,
			colours.white
		)

		status_buffer:render()
		if welcome_buffer_shown then
			welcome_buffer:render( nil, nil, math.floor( h + welcome_buffer_y_offset ) )
		end

		main_buffer
			:write( 0, 0, tostring( debug ) )
			:render_to_window( main_win )

		a = a + dt * rotation_speed
	end

	last_time = now
end

--[==[

DEAD CODE



	-- local org = rotation_speed
	-- local acc = -org / slowdown_duration
	-- local d = org * slowdown_duration + 0.5 * acc * slowdown_duration * slowdown_duration
	-- local d = org * slowdown_duration - 0.5 * org * slowdown_duration
	-- local d = 0.5 * org * slowdown_duration
	-- local target = math.pi * 1.85
	-- local wait = ( target - d ) / org

	-- debug = "wait (" .. d .. ")"
	-- local s = clock()
	-- while clock() - s < wait do
		-- coroutine.yield()
	-- end
	-- debug = "slowdown (" .. d .. ", " .. a .. ")"
	-- s = clock()
	-- while clock() - s < slowdown_duration do
		-- coroutine.yield()
		-- rotation_speed = ( 1 - ( clock() - s ) / slowdown_duration ) * org
	-- end
	-- debug = "slowdown"


--]==]
