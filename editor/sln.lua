
-- luacheck:globals os.pullEvent os.queueEvent

package.path = package.path .. ";/yelua/?.lua;/yelua/?/init.lua"

local f = io.open( "yelua/editor/ed.lua", "r" )
local contents = f:read "*a"
f:close()

-- FIXME: make this 5.2+ compatible
local ed = loadstring( contents, "ed.lua" )
setfenv( ed, getfenv( 1 ) )
local ok, err = ed{}

-- os.queueEvent "selene_exit"
-- if not ok then
-- 	print "Selene has encountered a fatal error and crashed:"
-- 	print( err )

-- 	os.pullEvent "selene_exit"
-- 	print "Press any key to continue..."
-- 	os.pullEvent "key_up"
-- end
