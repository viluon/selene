
-- luacheck:globals colours

local buffer = require "desktox".buffer
local vanilla_colours = colours
local colours = buffer.colours

local themes = {
	advanced = {
		background = colours.black;
		foreground = colours.white;
		primary    = colours.blue;
		secondary  = colours.grey;
		accent     = colours.yellow;
		warn       = colours.orange;
		err        = colours.red;

		code_line  = colours.light_grey;

		-- FIXME: we need a mapping
		caret      = vanilla_colours.purple;

		highlight = {
			keyword = colours.yellow;
			string = colours.red;
			control_character = colours.cyan;
			comment = colours.green;
			summary = colours.lime;
			number = colours.blue;
		};

		semantic_colours = {
			colours.pink,
			colours.light_blue,
			colours.brown,
			colours.blue,
			colours.white,
			colours.magenta
		}
	};
}

return themes
