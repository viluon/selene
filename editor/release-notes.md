
This is a draft of the various release posts which are to be published on ComputerCraft, OpenComputers, and, possibly, TIC-80 forums.

---

# Full power. Everywhere.

### The first truly intelligent Lua IDE
Selene is much more than just a source code editor.



*Note: it may be beneficial to inform users frequently and elaborately
about new features and improvements (hype hype hype). A draft of
such a notice follows.*

## Update v0.4.1
### (4th September 2018)

Dear users,

This update fixes two major bugs and four smaller issues.
Furthermore, Selene's already swift search features are now
even faster, thanks to improvements in the fuzzy matching
algorithm.

New features regarding smarter code completion and advanced
static analysis will start shipping in version v0.5.0, which
should be arriving next week. I will continously refine them
throughout this month at the next, in preparation for
the upcoming full release by the end of October.

Stay tuned!

~viluon
